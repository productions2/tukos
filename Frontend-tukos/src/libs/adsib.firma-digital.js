import axios from 'axios';

const ADSIB = {
  firmaDigital: {
    jacobitusFIDO: {
      urlBase: 'https://localhost:9000',
      async firmar(datos, seguimiento) {
        let respuesta;
        const pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        const pin = datos.pin;
        try {
          seguimiento('Firmando', 'Verificando conexión con el TOKEN');
          await axios.get(
            this.urlBase + '/api/token/connected'
          ).then((response) => respuesta = response.data);
          if (respuesta.finalizado === true && respuesta.datos.tokens.length > 0) {
            seguimiento('Firmando', 'Iniciando conexión con el TOKEN');
            const slot = respuesta.datos.tokens[0].slot;
            await axios.post(
              this.urlBase + '/api/token/data', { slot, pin }
            ).then((response) => respuesta = response.data);
            if (respuesta.finalizado === true) {
              seguimiento('Firmando', 'Obteniendo certificado del TOKEN');
              const alias = respuesta.datos.data_token.data[1].alias;
              seguimiento('Firmando', 'Firmando documento');
              await axios.post(
                this.urlBase + '/api/token/firmar_pdf', { slot, pin, alias, pdf: pdfBase64 }
              ).then((response) => respuesta = response.data);
              if (respuesta.finalizado === true) {
                const pdfFirmadoBase64 = respuesta.datos.pdf_firmado;
                seguimiento('Firmando', 'Cerrando conexión con el TOKEN');
                return pdfFirmadoBase64;
              } else {
                seguimiento('Firmando', 'El documento no se pudo firmar, intentelo nuevamente.');
              }
            } else {
              seguimiento('Firmando', 'No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
            }
          } else {
            seguimiento('Firmando', 'No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
          }
        } catch (error) {
          seguimiento('Firmando', error);
        }
      },
      async validar(datos, seguimiento) {
        let pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        seguimiento('Validando', 'Verificando firma(s) del documento');
        let respuesta;
        try {
          await axios.post(
            this.urlBase + '/api/validar_firma_pdf',
            { pdf: pdfBase64 }
          ).then((response) => respuesta = response.data);
          if (respuesta.finalizado === true && respuesta.datos !== null) {
            return JSON.stringify(respuesta.datos, null, 4);
          }
        } catch (error) {
          seguimiento('Validando', error);
        }
      },
    },
    jacobitusTotal: {
      urlBase: 'https://localhost:9000',
      async firmar(datos, seguimiento) {
        let pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        let respuesta;
        try {
          seguimiento('Firmando', 'Verificando conexión con el TOKEN');
          await axios.get(
            this.urlBase + '/api/token/connected'
          ).then((response) => respuesta = response.data);
          if (respuesta.finalizado === true && respuesta.datos.tokens.length > 0) {
            seguimiento('Firmando', 'Iniciando conexión con el TOKEN');
            let pin = datos.pin
            const slot = respuesta.datos.tokens[0].slot;
            await axios.post(
              this.urlBase + '/api/token/data', { slot, pin }
            ).then((response) => respuesta = response.data);
            if (respuesta.finalizado === true) {
              const alias = respuesta.datos.data_token.data[0].alias;
              seguimiento('Firmando', 'Firmando documento');
              await axios.post(
                this.urlBase + '/api/token/firmar_pdf', { slot, pin, alias, 'pdf': pdfBase64 }
              ).then((response) => respuesta = response.data);
              if (respuesta.finalizado === true) {
                const pdfFirmadoBase64 = respuesta.datos.pdf_firmado;
                seguimiento('Firmando', 'Cerrando conexión con el TOKEN');
                return pdfFirmadoBase64;
              } else {
                seguimiento('Firmando', 'El documento no se pudo firmar, intentelo nuevamente.');
              }
            } else {
              seguimiento('Firmando', 'No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
            }
          } else {
            seguimiento('Firmando', 'No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
          }
        } catch (error) {
          seguimiento('Firmando', error);
        }
      },
      async validar(datos, seguimiento) {
        const pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        seguimiento('Validando', 'Verificando firma(s) del documento');
        let respuesta;
        try {
          await axios.post(
            this.urlBase + '/api/validar_pdf',
            { pdf: pdfBase64 }
          ).then((response) => respuesta = response.data);
          if (respuesta.finalizado === true && respuesta.datos !== null) {
            return JSON.stringify(respuesta.datos, null, 4);
          }
        } catch (error) {
          seguimiento('Validando', error);
        }
      },
    },
    asistenteFirmador: {
      urlBase: 'https://localhost:3200',
      async firmar(datos, seguimiento) {
        let respuesta;
        const pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        const pin = datos.pin;
        try {
          seguimiento('Firmando', 'Verificando conexión con el TOKEN');
          await axios.get(
            this.urlBase + '/tokens'
          ).then((response) => respuesta = response.data);
          if (respuesta.finalizado === true && respuesta.datos.length > 0) {
            seguimiento('Firmando', 'Iniciando conexión con el TOKEN');
            await axios.get(
              this.urlBase + '/start?pin=' + pin
            ).then((response) => respuesta = response.data);
            if (respuesta.finalizado === true) {
              seguimiento('Firmando', 'Obteniendo certificado del TOKEN');
              await axios.get(
                this.urlBase + '/certs'
              ).then((response) => respuesta = response.data);
              if (respuesta.finalizado === true) {
                const alias = respuesta.datos[0].alias;
                const body = JSON.stringify({ 'nombre_archivo': 'documento.pdf', 'alias': alias, 'pdf_base64': pdfBase64 });
                seguimiento('Firmando', 'Firmando documento');
                await axios.post(
                  this.urlBase + '/sign', body
                ).then((response) => respuesta = response.data);
                if (respuesta.finalizado === true) {
                  const pdfFirmadoBase64 = respuesta.datos.pdf_base64;
                  seguimiento('Firmando', 'Cerrando conexión con el TOKEN');
                  await axios.get(
                    this.urlBase + '/finish'
                  ).then((response) => respuesta = response.data);
                  return pdfFirmadoBase64;
                } else {
                  seguimiento('Firmando', 'El documento no se pudo firmar, intentelo nuevamente.');
                }
              } else {
                seguimiento('Firmando', 'No se pudo obtener el certificado de su TOKEN, intentelo nuevamente.');
                respuesta = await axios.get(
                  this.urlBase + '/finish'
                );
              }
            } else {
              seguimiento('Firmando', 'No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
            }
          } else {
            seguimiento('Firmando', 'No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
          }
        } catch (error) {
          seguimiento('Firmando', error);
        }
      },
    },
    firmatic: {
      urlBase: 'https://localhost:4637',
      async firmar(datos, seguimiento) {
        const ci = datos.ci;
        let pdfBase64 = datos.archivoPdfBase64.replace('data:application/pdf;base64,', '');
        seguimiento('Firmando', 'Iniciando');
        const body = ci && ci !== '' ?
          { ci, archivo: [{ base64: pdfBase64, name: 'temporal.pdf' }], format: 'pades', language: 'es' } :
          { archivo: [{ base64: pdfBase64, name: 'temporal.pdf' }], format: 'pades', language: 'es' };
        let respuesta;
        try {
          await axios.post(
            this.urlBase + '/sign',
            body
          ).then((response) => respuesta = response.data);
          if (respuesta.files) {
            return respuesta.files[0].base64;
          } else {
            seguimiento('Firmando', respuesta.message);
          }
        } catch (error) {
          seguimiento('Firmando', error);
        }
      },
    }
  }
}

export default ADSIB
