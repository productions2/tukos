import { createRouter, createWebHistory } from "vue-router";
import store from "vuex";
import Profile from "@/views/Profile.vue";
import SignIn from "@/views/SignIn.vue";
import RegistroPersona from "@/views/RegistroPersona.vue"
import RegistroHabitacion from "@/views/RegistroHabitacion.vue"
import RegistroActivos from "@/views/RegistroActivos.vue"
import Principal from "@/views/Principal.vue"
import Caja from "@/views/Caja.vue"
import Admin from "@/views/Admin.vue"
import Personal from '@/views/Personal.vue';
import Shop from "@/views/Shop.vue"
import pruebas from '@/views/pruebas.vue'

const routes = [
  {
    path: "/",
    name: "/Sign In",
    components: {default: SignIn}
  },

  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/sign-in",
    name: "Sign In",
    component: SignIn,
  },
  {
    path: "/Principal",
    name: "Principal",
    component: Principal,
  },
  {
    path: "/Shop",
    name: "Shop",
    component: Shop,
  },
  {
    path: "/Caja",
    name: "Caja",
    component: Caja,
  },
  {
    path: "/RegistroPersona",
    name: "Registro-Persona",
    component: RegistroPersona,
  },
  {
    path: "/RegistroHabitacion",
    name: "Registro-Habitacion",
    component: RegistroHabitacion,
  },
  {
    path: "/RegistroActivos",
    name: "Registro-Activos",
    component: RegistroActivos,
  },
  {
    path: "/Admin",
    name: "Admin",
    component: Admin,
  },
  {
    path: "/Personal",
    name: "Personal",
    component: Personal,
  },
  {
    path: "/prubeas",
    name: "pruebas",
    component: pruebas,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  linkActiveClass: "active",
});

router.beforeEach((to, from, next)=>{
  const rutaProtegida = to.matched.some(record=> record.meta.requireAuth)
  if(rutaProtegida && store.mapState.token === null)next({name: "Sign In"})
  else next()
})
export default router;
