import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "./assets/css/nucleo-icons.css";
import "./assets/css/nucleo-svg.css";
import SoftUIDashboard from "./soft-ui-dashboard";
import axios from "axios";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/carousel";
import "bootstrap/js/dist/collapse";

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

const appInstance = createApp(App);
const api = axios.create({
  baseURL: "http://localhost:3000/api/",
  headers: {
    "Content-type": "application/json",
    "Access-Control-Allow-Origin": "*"
    //"auth-token": localStorage.getItem("token2")
  }
});
const firma = axios.create({
  baseURL: "http://192.168.100.16:3001/api/",
  headers: {
    "Content-Type": "multipart/form-data",
    "Access-Control-Allow-Origin": "*"
  },
  //responseType: "blob"
});

appInstance.use(Toast);
appInstance.config.globalProperties.$api = { ...api };
appInstance.config.globalProperties.$firma = { ...firma };
appInstance.use(store);
appInstance.use(router);
appInstance.use(SoftUIDashboard);
appInstance.mount("#app");
