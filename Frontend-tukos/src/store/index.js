import { createStore } from "vuex";
import bootstrap from "bootstrap/dist/js/bootstrap.min.js";
import axios from "axios";
export default createStore({
  state: {
    hideConfigButton: false,
    isPinned: true,
    showConfig: false,
    isTransparent: "",
    isRTL: false,
    color: "",
    isNavFixed: false,
    isAbsolute: false,
    showNavs: true,
    showSidenav: true,
    showNavbar: true,
    showFooter: true,
    showMain: true,
    navbarFixed:
      "position-sticky blur shadow-blur left-auto top-1 z-index-sticky px-0 mx-4",
    absolute: "position-absolute px-4 mx-0 w-100 z-index-2",
    bootstrap,
    token: null,
    id_usuario: null,
    estado_caja: false,
    id_caja: null,
    rol: null,
    id_asistencia: null,
  },
  mutations: {
    toggleConfigurator(state) {
      state.showConfig = !state.showConfig;
    },
    navbarMinimize(state) {
      const sidenav_show = document.querySelector(".g-sidenav-show");
      if (sidenav_show.classList.contains("g-sidenav-hidden")) {
        sidenav_show.classList.remove("g-sidenav-hidden");
        sidenav_show.classList.add("g-sidenav-pinned");
        state.isPinned = true;
      } else {
        sidenav_show.classList.add("g-sidenav-hidden");
        sidenav_show.classList.remove("g-sidenav-pinned");
        state.isPinned = false;
      }
    },
    sidebarType(state, payload) {
      state.isTransparent = payload;
    },
    cardBackground(state, payload) {
      state.color = payload;
    },
    navbarFixed(state) {
      if (state.isNavFixed === false) {
        state.isNavFixed = true;
      } else {
        state.isNavFixed = false;
      }
    },
    toggleEveryDisplay(state) {
      state.showNavbar = !state.showNavbar;
      state.showSidenav = !state.showSidenav;
      state.showFooter = !state.showFooter;
    },
    toggleHideConfig(state) {
      state.hideConfigButton = !state.hideConfigButton;
    },
    setToken(state, payload) {
      state.token = payload;
    },
    setIdUsuario(state, payload) {
      state.id_usuario = payload;
    },
    setStatus(state, payload) {
      state.estado_caja = payload;
    },
    setIdCaja(state, payload) {
      state.id_caja = payload;
    },
    setRol(state, payload) {
      state.rol = payload;
    },
    setIdAsistencia(state, payload) {
      state.id_asistencia = payload;
    },
  },
  actions: {
    toggleSidebarColor({ commit }, payload) {
      commit("sidebarType", payload);
    },
    setCardBackground({ commit }, payload) {
      commit("cardBackground", payload);
    },
    async login({ commit }, usuario) {
      try {
        //http://10.10.165.84:3000/api/persona
        const res = await axios.post(
          "http://localhost:3000/api/usuario/login",
          usuario
        );
        if (!res.data.err) {
          const usuariodb = res.data.data.token;
          const id = res.data.id;
          const { data } = await axios.get(
            "http://localhost:3000/api/usuario/" + id
          );
          const rol = data.rol;
          commit("setToken", usuariodb);
          commit("setIdUsuario", id);
          commit("setRol", rol);
          localStorage.setItem("token2", usuariodb);
          localStorage.setItem("idUsuario", id);
          localStorage.setItem("rol", rol);
          this.token = usuariodb;
          return usuariodb;
        } else {
          alert(res.data.err);
        }
      } catch (error) {
        alert(error.message);
      }
    },
    status({ commit }, info) {
      commit("setStatus", info.estado);
      commit("setIdCaja", info.id_caja);
    },
    obtenerToken({ commit }) {
      if (localStorage.getItem("token2")) {
        commit("setToken", localStorage.getItem("token2"));
        commit("setRol", localStorage.getItem("rol"));
        commit("setIdUsuario", localStorage.getItem("idUsuario"));
        commit("setIdAsistencia", localStorage.getItem("idAsistencia"));
      } else {
        commit("setToken", null);
        commit("setIdUsuario", null);
        commit("setStatus", false);
        commit("setIdCaja", null);
      }
    },
    eliminarCredenciales({ commit }) {
      localStorage.removeItem("token2");
      localStorage.removeItem("idUsuario");
      localStorage.removeItem("rol");
      localStorage.removeItem("idAsistencia");
      commit("setToken", null);
      commit("setRol", null);
      commit("setIdUsuario", null);
      commit("setIdAsistencia", null);
    },
    async marcarAsistencia({ commit }, id) {
      try {
        const {
          data,
        } = await axios.post("http://localhost:3000/api/reporte_personal", {
          userId: id,
        });
        commit("setIdAsistencia", data.reporte.id);
        localStorage.setItem("idAsistencia", data.reporte.id);
        return data.reporte;
      } catch (error) {
        alert(error.message);
      }
    },
  },
  getters: {
    getToken(state) {
      console.log(state.token);
      return state.token;
    },
  },
});
