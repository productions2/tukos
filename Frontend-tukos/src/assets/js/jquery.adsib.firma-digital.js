/*!jQuery ADSIB Firma Digital*/
/**
 * Libreria para integración con software de firma digital
 * 
 * Version: 2.0.0
 * Dependencias:
 *  - jQuery v1.7+
 *  - axios v0.19.2+
 *
 * Autor: ADSIB - Juan Carlos Paredes (jparedes@adsib.gob.bo)
 * Licencia: LPG-Bolivia
 *
 */

(function () {
    jQuery.extend({
        _funciones: {
            consultarServicio: (urlBase, ruta, tipo, datos) => {
                return new Promise((resolve, reject) => {
                    try {
                        axios({
                            url: urlBase + ruta,
                            method: tipo,
                            data: datos,
                            responseType: 'json',
                        }).then(response => {
                            resolve(response.data);
                        }).catch(error => {
                            reject(error);
                        });
                    } catch (error) {
                        if (error.message === 'axios is not defined') {
                            $._funciones.progreso('Proceso detenido', 'Se necesita la libreria AXIOS.');
                        }
                        resolve(error);
                    }
                });
            },
            prompt: () => {
                return new Promise((resolve, reject) => {
                    $('body').append('<div id="prompt-adsib-firma-digital"><div id="pin-adsib-firma-digital"><div id="pin-adsib-firma-digital-contenedor"><p><label>Introduzca su PIN:</label></p><p><input id="pin-textbox" type="password" /></p><p><input id="pin-button" type="button" value="Continuar" /></p></div></div></div>');
                    $('#pin-textbox').focus();
                    $('#pin-textbox').on('keypress', function (e) {
                        if (e.which == 13) {
                            $('#pin-button').click();
                        }
                    });
                    $('#pin-button').on('click', function () {
                        resolve($('#pin-textbox').val());
                        $('#prompt-adsib-firma-digital').hide();
                        $('#prompt-adsib-firma-digital').remove();
                    });
                    $('#prompt-adsib-firma-digital').show();
                });
            },
            bloquear: () => {
                $('body').css('cursor', 'progress');
                $('body').append('<div id="overlay-adsib-firma-digital" style=""><div id="titulo-adsib-firma-digital" style="">Firmando</div><div id="operacion-contenedor-adsib-firma-digital"><br><span id="operacion-adsib-firma-digital">Iniciando</span></div></div>');
                $('#overlay-adsib-firma-digital').show();
            },
            progreso: (titulo, operacion) => {
                $('#titulo-adsib-firma-digital').html(titulo);
                $('#operacion-adsib-firma-digital').html(operacion);
            },
            desbloquear: () => {
                $('body').css('cursor', 'default');
                $('#overlay-adsib-firma-digital').hide();
                $('#overlay-adsib-firma-digital').remove();
            },
            obtenerDatos: (tipo) => {
                switch(tipo) {
                    case 'firma': {
                        if ($('.firma-digital-ci')[0]) {
                            return {
                                ci: $('.firma-digital-ci')[0].value,
                                archivoBase64: $('.firma-digital-pdf-base64').val()
                            };
                        } else {
                            return {
                                archivoBase64: $('.firma-digital-pdf-base64').val()
                            };
                        }
                        break;
                    }
                    case 'validacion': {
                        return {
                            archivoBase64: $('.firma-digital-pdf-base64-firmado').val()
                        };
                    }
                }
            }
        },
        _jacobitusFIDO: {
            urlBase: 'https://localhost:9000',
            firmar: (pdfBase64) => {
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Firmando', 'Verificando conexión con el TOKEN');
                    $._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/api/token/connected', 'GET', null).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos.tokens.length > 0) {
                            $._funciones.progreso('Firmando', 'Iniciando conexión con el TOKEN');
                            $._funciones.prompt().then((pin) => {
                                const slot = respuesta.datos.tokens[0].slot;
                                $._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/api/token/data', 'POST', { pin, slot }).then((respuesta) => {
                                    if (respuesta.finalizado === true) {
                                        $._funciones.progreso('Firmando', 'Obteniendo certificado del TOKEN');
                                        const alias = respuesta.datos.data_token.data[1].alias;
                                        $._funciones.progreso('Firmando', 'Firmando documento');
                                        pdfBase64 = pdfBase64.replace('data:application/pdf;base64,', '');
                                        $._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/api/token/firmar_pdf', 'POST', { pin, slot, alias, pdf: pdfBase64 }).then((respuesta) => {
                                            if (respuesta.finalizado === true) {
                                                const pdfFirmadoBase64 = respuesta.datos.pdf_firmado;
                                                //$._funciones.progreso('Firmando', 'Cerrando conexión con el TOKEN');
                                                //$._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/shutdown_service', 'GET', null);
                                                resolve(pdfFirmadoBase64);
                                                $._funciones.desbloquear();
                                            } else {
                                                reject('El documento no se pudo firmar, intentelo nuevamente.');
                                                $._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/api/shutdown_service', 'GET', null);
                                                $._funciones.desbloquear();
                                            }
                                        }).catch((error) => {
                                            reject('/api/token/firmar_pdf: ' + error);
                                            $._funciones.desbloquear();
                                        });
                                    } else {
                                        reject('No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
                                        $._funciones.desbloquear();
                                    }
                                }).catch((error) => {
                                    reject('/api/token/data: ' + error);
                                    $._funciones.desbloquear();
                                });
                            });
                        } else {
                            reject('No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._jacobitusFIDO.urlBase);
                        } else {
                            reject('/api/token/connected: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            },
            validarFirma: (pdfBase64) => {
                pdfBase64 = pdfBase64.replace('data:application/pdf;base64,', '');
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Validando', 'Verificando firma(s) del documento');
                    $._funciones.consultarServicio($._jacobitusFIDO.urlBase, '/api/validar_firma_pdf', 'POST', { pdf: pdfBase64 }).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos !== null) {
                            resolve(respuesta.datos);
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._jacobitusFIDO.urlBase);
                        } else {
                            reject('/api/validar_firma_pdf: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            }
        },
        _jacobitusTotal: {
            urlBase: 'https://localhost:9000',
            firmar: (pdfBase64) => {
                if (pdfBase64.startsWith('data:application/pdf;base64,')) {
                    pdfBase64 = pdfBase64.replace('data:application/pdf;base64,', '');
                }
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Firmando', 'Verificando conexión con el TOKEN');
                    $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/connected', 'GET', null).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos.tokens.length > 0) {
                            $._funciones.progreso('Firmando', 'Iniciando conexión con el TOKEN');
                            $._funciones.prompt().then((pin) => {
                                const slot = respuesta.datos.tokens[0].slot;
                                const body = { slot, pin };
                                $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/data', 'POST', body).then((respuesta) => {
                                    if (respuesta.finalizado === true) {
                                        const alias = respuesta.datos.data_token.data[0].alias;
                                        const body = { slot, pin, alias, 'pdf': pdfBase64 };
                                        $._funciones.progreso('Firmando', 'Firmando documento');
                                        $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/firmar_pdf', 'POST', body).then((respuesta) => {
                                            if (respuesta.finalizado === true) {
                                                const pdfFirmadoBase64 = respuesta.datos.pdf_firmado;
                                                $._funciones.progreso('Firmando', 'Cerrando conexión con el TOKEN');
                                                resolve(pdfFirmadoBase64);
                                                $._funciones.desbloquear();
                                            } else {
                                                reject('El documento no se pudo firmar, intentelo nuevamente.');
                                                $._funciones.desbloquear();
                                            }
                                        }).catch((error) => {
                                            reject('/api/token/firmar_pdf: ' + error.message);
                                            $._funciones.desbloquear();
                                        });
                                    } else {
                                        //$._funciones.progreso('Error', respuesta.mensaje);
                                        reject('No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
                                        $._funciones.desbloquear();
                                    }
                                }).catch((error) => {
                                    reject('/api/token/data: ' + error.message);
                                    $._funciones.desbloquear();
                                });
                            });
                        } else {
                            reject('No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._jacobitusTotal.urlBase);
                        } else {
                            reject('/api/token/connected: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            },
            firmarLote: (lotePdfBase64) => {
                lotePdfBase64 = lotePdfBase64.map((item) => {
                    if (item.pdfBase64.startsWith('data:application/pdf;base64,')) {
                        item.pdfBase64 = item.pdfBase64.replace('data:application/pdf;base64,', '');
                    }
                    return { id: item.id, pdf: item.pdfBase64 };
                });
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Firmando', 'Verificando conexión con el TOKEN');
                    $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/connected', 'GET', null).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos.tokens.length > 0) {
                            $._funciones.progreso('Firmando', 'Iniciando conexión con el TOKEN');
                            $._funciones.prompt().then((pin) => {
                                const slot = respuesta.datos.tokens[0].slot;
                                const body = { slot, pin };
                                $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/data', 'POST', body).then((respuesta) => {
                                    if (respuesta.finalizado === true) {
                                        const alias = respuesta.datos.data_token.data[0].alias;
                                        const body = { slot, pin, alias, 'pdfs': lotePdfBase64 };
                                        $._funciones.progreso('Firmando', 'Firmando documento');
                                        $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/token/firmar_lote_pdfs', 'POST', body).then((respuesta) => {
                                            if (respuesta.finalizado === true) {
                                                let pdfsFirmadosBase64 = respuesta.datos.pdfs_firmados;
                                                pdfsFirmadosBase64 = pdfsFirmadosBase64.map((item) => {
                                                    return { id: item.id, pdfFirmadoBase64: item.pdf_firmado }
                                                });
                                                $._funciones.progreso('Firmando', 'Cerrando conexión con el TOKEN');
                                                resolve(pdfsFirmadosBase64);
                                                $._funciones.desbloquear();
                                            } else {
                                                reject('El documento no se pudo firmar, intentelo nuevamente.');
                                                $._funciones.desbloquear();
                                            }
                                        }).catch((error) => {
                                            reject('/api/token/firmar_pdf: ' + error.message);
                                            $._funciones.desbloquear();
                                        });
                                    } else {
                                        //$._funciones.progreso('Error', respuesta.mensaje);
                                        reject('No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
                                        $._funciones.desbloquear();
                                    }
                                }).catch((error) => {
                                    reject('/api/token/data: ' + error.message);
                                    $._funciones.desbloquear();
                                });
                            });
                        } else {
                            reject('No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._jacobitusTotal.urlBase);
                        } else {
                            reject('/api/token/connected: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            },
            validarFirma: (pdfBase64) => {
                pdfBase64 = pdfBase64.replace('data:application/pdf;base64,', '');
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Validando', 'Verificando firma(s) del documento');
                    $._funciones.consultarServicio($._jacobitusTotal.urlBase, '/api/validar_pdf', 'POST', { pdf: pdfBase64 }).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos !== null) {
                            resolve(respuesta.datos);
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._jacobitusTotal.urlBase);
                        } else {
                            reject('/api/validar_pdf: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            }
        },
        _asistenteFirmador: {
            urlBase: 'https://localhost:3200',
            firmar: (pdfBase64) => {
                pdfBase64 = pdfBase64.replace('data:application/pdf;base64,', '');
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Firmando', 'Verificando conexión con el TOKEN');
                    $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/tokens', 'GET', null).then((respuesta) => {
                        if (respuesta.finalizado === true && respuesta.datos.length > 0) {
                            $._funciones.progreso('Firmando', 'Iniciando conexión con el TOKEN');
                            $._funciones.prompt().then((pin) => {
                                $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/start?pin=' + pin, 'GET', null).then((respuesta) => {
                                    if (respuesta.finalizado === true) {
                                        $._funciones.progreso('Firmando', 'Obteniendo certificado del TOKEN');
                                        $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/certs', 'GET', null).then((respuesta) => {
                                            if (respuesta.finalizado === true) {
                                                const alias = respuesta.datos[0].alias;
                                                const body = JSON.stringify({ 'nombre_archivo': 'documento.pdf', 'alias': alias, 'pdf_base64': pdfBase64 });
                                                $._funciones.progreso('Firmando', 'Firmando documento');
                                                $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/sign', 'POST', body).then((respuesta) => {
                                                    if (respuesta.finalizado === true) {
                                                        const pdfFirmadoBase64 = respuesta.datos.pdf_base64;
                                                        $._funciones.progreso('Firmando', 'Cerrando conexión con el TOKEN');
                                                        $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/finish', 'GET', null);
                                                        resolve(pdfFirmadoBase64);
                                                        $._funciones.desbloquear();
                                                    } else {
                                                        reject('El documento no se pudo firmar, intentelo nuevamente.');
                                                        $._funciones.desbloquear();
                                                    }
                                                }).catch((error) => {
                                                    reject('/sign: ' + error.message);
                                                    $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/finish', 'GET', null);
                                                    $._funciones.desbloquear();
                                                });
                                            } else {
                                                reject('No se pudo obtener el certificado de su TOKEN, intentelo nuevamente.');
                                                $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/finish', 'GET', null);
                                                $._funciones.desbloquear();
                                            }
                                        }).catch((error) => {
                                            reject('/certs: ' + error.message);
                                            $._funciones.consultarServicio($._asistenteFirmador.urlBase, '/finish', 'GET', null);
                                            $._funciones.desbloquear();
                                        });
                                    } else {
                                        //$._funciones.progreso('Error', respuesta.mensaje);
                                        reject('No se ha podido iniciar la sesión de firma de documentos, intentelo nuevamente.');
                                        $._funciones.desbloquear();
                                    }
                                }).catch((error) => {
                                    reject('/start: ' + error.message);
                                    $._funciones.desbloquear();
                                });
                            });
                        } else {
                            reject('No se ha detectado su TOKEN, conectelo y vuelva a intentarlo nuevamente.');
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._asistenteFirmador.urlBase);
                        } else {
                            reject('/tokens: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            }
        },
        _firmatic: {
            urlBase: 'https://localhost:4637',
            firmar: (ci, pdfBase64) => {
                return new Promise((resolve, reject) => {
                    $._funciones.bloquear();
                    $._funciones.progreso('Firmando', 'Iniciando');
                    const body = ci && ci !== '' ? { ci, archivo: [{ base64: pdfBase64, name: 'temporal.pdf' }], format: 'pades', language: 'es' } : { archivo: [{ base64: pdfBase64, name: 'temporal.pdf' }], format: 'pades', language: 'es' };
                    $._funciones.consultarServicio($._firmatic.urlBase, '/sign', 'POST', body).then((respuesta) => {
                        if (respuesta.files) {
                            const pdfFirmadoBase64 = respuesta.files[0].base64;
                            resolve(pdfFirmadoBase64);
                            $._funciones.desbloquear();
                        } else {
                            reject(respuesta.message);
                            $._funciones.desbloquear();
                        }
                    }).catch((error) => {
                        if (error == 'Error: Network Error') {
                            reject('El firmador no se encuentra en ejecución o no esta configurado para su navegador.');
                            window.open($._firmatic.urlBase);
                        } else {
                            reject('/: ' + error);
                        }
                        $._funciones.desbloquear();
                    });
                });
            }
        },
        ADSIB: {
            firmaDigital: {
                jacobitusFIDO: {
                    firmar: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('firma') : datos;
                        return $._jacobitusFIDO.firmar(datos.archivoBase64);
                    },
                    validar: function(datos = null) {
                        datos = (datos === null) ? $._funciones.obtenerDatos('validacion') : datos;
                        return $._jacobitusFIDO.validarFirma(datos.archivoBase64);
                    }
                },
                jacobitusTotal: {
                    firmar: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('firma') : datos;
                        return $._jacobitusTotal.firmar(datos.archivoBase64);
                    },
                    firmarLote: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('firma-lote') : datos;
                        return $._jacobitusTotal.firmarLote(datos.loteArchivoBase64);
                    },
                    validar: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('validacion') : datos;
                        return $._jacobitusTotal.validarFirma(datos.archivoBase64);
                    }
                },
                asistenteFirmador: {
                    firmar: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('firma') : datos;
                        return $._asistenteFirmador.firmar(datos.archivoBase64);
                    }
                },
                firmatic: {
                    firmar: (datos = null) => {
                        datos = (datos === null) ? $._funciones.obtenerDatos('firma') : datos;
                        return $._firmatic.firmar(datos.ci, datos.archivoBase64);
                    }
                }
            }
        },
    });
})();
