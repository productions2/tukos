<!DOCTYPE html>
<html>
<head>
    <title>PDF Firmado</title>
</head>
<body>
    <p>El PDF ha sido firmado correctamente. Puedes descargarlo aquí:</p>
    <a href="{{ route('download-signed-pdf', $signedPdfPath) }}">Descargar PDF Firmado</a>
</body>
</html>
