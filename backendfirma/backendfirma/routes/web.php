<?php

use App\Http\Controllers\FirmaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/csrf-token', function () {
    return response()->json(['csrfToken' => csrf_token()]);
});

Route::get('/datos',[FirmaController::class, 'datos']);
Route::get('/generate-keys', [FirmaController::class, 'generateKeys']);
Route::get('/generate-signed-pdf', [FirmaController::class, 'generateSignedPdf']);
Route::get('/upload-pdf', [FirmaController::class, 'showForm'])->name('show-form');
Route::post('/process-pdf', [FirmaController::class, 'processForm'])->name('process-form');
Route::get('/download-signed-pdf/{signedPdfPath}', [FirmaController::class, 'downloadSignedPdf'])->name('download-signed-pdf');
Route::post('/firmar-pdf', [FirmaController::class, 'firmarPDF']);
Route::get('/ultimopdf',[FirmaController::class, 'ultimopdf']);
