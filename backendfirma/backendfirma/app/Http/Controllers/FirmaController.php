<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCPDF;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class FirmaController extends Controller
{
    public function datos(){
        $hola = "hola mundo de vue";
        return response()->json(['message' => $hola]);
    }

    public function generateKeys()
    {
        // Load PKCS#12 file
        $pkcs12Path = public_path('softoken.p12');
        $pkcs12Password = 'Gatoblanco6'; // Replace with your PKCS#12 password
        $pkcs12Data = file_get_contents($pkcs12Path);
        $certificates = [];
        if (openssl_pkcs12_read($pkcs12Data, $certificates, $pkcs12Password)) {
            // Save the private key to key.pem
            $privateKey = $certificates['pkey'];
            file_put_contents(public_path('firma/key.pem'), $privateKey);

            // Save the certificate to cert.crt
            $certificate = $certificates['cert'];
            file_put_contents(public_path('firma/cert.crt'), $certificate);

            return "Keys generated successfully!";
        } else {
            return "Error reading PKCS#12 file.";
        }
    }

    public function generateSignedPdf()
    {
        $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
        $info = array(
            'Name' => 'TCPDF'
        );
        $certificate = 'file://' . public_path('firma/cert.crt');
        $primaryKey = 'file://' . public_path('firma/key.pem');

        $pdf->AddPage();
        $pdf->writeHTML('<b>Este es un documento firmado digitalmente usando TCPDF en Laravel</b>', true, 0, true, 0);
        $pdf->setSignature($certificate, $primaryKey, 'Gatoblanco6', '', 2, $info);

        $pdfContent = $pdf->Output('', 'S');

        // You can return the PDF as a response or save it to a file
        return response($pdfContent)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline; filename="signed_pdf.pdf"');
    }

    public function showForm()
    {
        return view('firma.form');
    }

    public function processForm(Request $request){
        $request->validate([
            'pdf_file' => 'required|mimes:pdf|max:9048', // Ajusta el tamaño máximo según tus necesidades
        ]);

        $pdfFile = $request->file('pdf_file');

        // Rutas de los archivos de certificado y clave privada
        $certificate = 'file://' . public_path('firma/cert.crt');
        $primaryKey = 'file://' . public_path('firma/key.pem');

        // Ruta del archivo PDF firmado
        $signedPdfPath = public_path('pdfs_firmados') . '/' . time() . '_signed.pdf';

        // Crear una instancia de Fpdi
        $pdf = new Fpdi();

        // Importar el contenido del PDF original
        $pageCount = $pdf->setSourceFile($pdfFile->path());

        // Agregar páginas del PDF original al nuevo PDF
        for ($pageNumber = 1; $pageNumber <= $pageCount; $pageNumber++) {
            $templateId = $pdf->importPage($pageNumber);
            $size = $pdf->getTemplateSize($templateId);
            $pdf->AddPage($size);
            $pdf->useTemplate($templateId);

            // Agregar la firma utilizando TCPDF
            if ($pageNumber === 1) { // Agregar firma solo en la primera página
                $info = array(
                    'Name' => 'TCPDF'
                );
                $pdf->SetAlpha(0.0); // Hacer la firma invisible
                $pdf->writeHTML('<b>Firma digital</b>', true, false, false, false, '');
                $pdf->SetAlpha(1.0); // Restaurar la transparencia
                $pdf->setSignature($certificate, $primaryKey, 'Gatoblanco6', '', 2, $info);
            }
        }

        // Guardar el PDF firmado en el servidor
        $pdf->Output($signedPdfPath, 'F');

        return response()->json(['signedPdfPath' => $signedPdfPath]);
        //return view('firma.download', ['signedPdfPath' => $signedPdfPath]);
    }

    public function firmarPDF(Request $request)
    {
        $pdfPath = $request->input('pdf_path'); // Obtener la ruta del PDF generado

        // Rutas de los archivos de certificado y clave privada
        $certificate = 'file://' . public_path('firma/cert.crt');
        $primaryKey = 'file://' . public_path('firma/key.pem');

        // Ruta del archivo PDF firmado
        $signedPdfPath = public_path('pdfs_firmados') . '/' . time() . '_signed.pdf';

        // Crear una instancia de Fpdi
        $pdf = new Fpdi();

        // Importar el contenido del PDF original
        $pageCount = $pdf->setSourceFile($pdfPath);

        // Agregar páginas del PDF original al nuevo PDF
        for ($pageNumber = 1; $pageNumber <= $pageCount; $pageNumber++) {
            $templateId = $pdf->importPage($pageNumber);
            $size = $pdf->getTemplateSize($templateId);
            $pdf->AddPage($size);
            $pdf->useTemplate($templateId);

            // Agregar la firma utilizando TCPDF
            if ($pageNumber === 1) { // Agregar firma solo en la primera página
                $info = array(
                    'Name' => 'TCPDF'
                );
                $pdf->SetAlpha(0.0); // Hacer la firma invisible
                $pdf->writeHTML('<b>Firma digital</b>', true, false, false, false, '');
                $pdf->SetAlpha(1.0); // Restaurar la transparencia
                $pdf->setSignature($certificate, $primaryKey, 'Gatoblanco6', '', 2, $info);
            }
        }

        // Guardar el PDF firmado en el servidor
        $pdf->Output($signedPdfPath, 'F');

        return response()->json(['signedPdfPath' => $signedPdfPath]);
    }

    public function ultimopdf()
    {
        // Ruta a la carpeta de PDFs firmados
        $rutaCarpeta = public_path('pdfs_firmados');

        // Obtener la lista de archivos PDF en la carpeta
        $archivosPDF = glob($rutaCarpeta . '/*.pdf');

        // Verificar si hay archivos en la carpeta
        if (empty($archivosPDF)) {
            abort(404, 'No se encontraron archivos PDF.');
        }

        // Ordenar la lista de archivos por fecha de modificación en orden descendente
        usort($archivosPDF, function ($a, $b) {
            return filemtime($b) - filemtime($a);
        });

        // Obtener la ruta al archivo PDF más reciente
        $rutaArchivo = $archivosPDF[0];

        // Obtener solo el nombre del archivo
        $nombreArchivo = basename($rutaArchivo);

        // Obtener el contenido del archivo
        $contenido = file_get_contents($rutaArchivo);

        // Devolver el contenido del archivo como respuesta con el tipo de contenido adecuado
        return Response::make($contenido, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $nombreArchivo . '"',
        ]);
    }

}
