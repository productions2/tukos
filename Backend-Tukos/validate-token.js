const jwt = require("jsonwebtoken")

const verifyToken = (req, res, next)=>{
    const token =  req.header("auth-token")
    if(!token) return res.json({error : "Acceso denegado"})
    try {
        const verified = jwt.verify(token, "secret")
        req.user = verified
        next()
    } catch (error) {
        res.json({error: "Token invalido"})
    }
}

module.exports = verifyToken