const { Pool } = require("pg");
const { Sequelize } = require("sequelize");

const config = require("./configurations/configurations");

const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);

const URI = `postgres://${USER}:${PASSWORD}@${config.dbHost}:${config.dbPort}/${config.dbName}`;

const sequelize = new Sequelize(URI);

//setupModels(sequelize);

// hara una sincromizacion y sequelise ira a crear la estructura
// sequelize.sync();

module.exports = { sequelize };

/*exports.client = new Pool({
    user: "postgres",
    host: "localhost", 
    database: "tukosdb",
    password: "admin",
    port: 5432
})*/
