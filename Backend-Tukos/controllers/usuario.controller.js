const joi = require("joi")
const{Users} = require("../models/user.model")
const bcrypt = require("bcrypt")
const jwt =require("jsonwebtoken")



exports.ListarUsuario= async(req,res)=>{
    const usuarios = await Users.findAll()
    res.json(usuarios)
}

exports.BuscarUsuario = async(req,res)=>{
    const {id} = req.params
    const usuario = await Users.findOne({where:{id}})
    res.json(usuario)
}
exports.CrearUsuario = async(req,res)=>{

    //const emailExist = await db.client.query("SELECT * FROM users WHERE name=$1",[value.name])
    try {
        const {email,password,nombre_completo,ci,imagen,estado,rol} = req.body
        const salt = await bcrypt.genSalt(10)
        const pass = await bcrypt.hash(password,salt)
        const newUsuario = await Users.create({
            email,
            password: pass,
            nombre_completo,
            ci,
            estado,
            rol,
            imagen,
            fecha_registro: Date.now()
        })
        res.json({
            error: null,
            usuario: newUsuario,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
    
}

exports.LoginUsuario = async(req, res)=>{
    const schemaLogin = joi.object({
        email: joi.string().min(6).max(30).email({minDomainSegments:2,tlds:{allow:["com"]}}).required(),
        password: joi.string().max(30).required(),
    })

    const {error, value} = schemaLogin.validate(req.body)

    if(error)return res.json({"err": error.details[0].message})

    const user =  await Users.findOne({where:{email: value.email}})
    if(!user)return res.json({"err": "El usuario "+value.email+" no existe"})

    const validPass =  await bcrypt.compare(value.password, user.password)
    console.log(value.password)
    console.log(user.password)
    if(!validPass) return res.json({"err": "Contraseña incorrecta"})

    if(user.estado!="Habilitado")return res.json({"err": `El usuario ${value.email} ha sido inhabilitado contactese con un administrador`})

    else{
        var token = jwt.sign({
            email: value.email,
        }, "secret")

        res.header("auth-token", token).json({
            error: null,
            id: user.id,
            data: {token}
        })
    }    
}

exports.ActualizarInfoPersonal = async (req, res)=>{
    try {
        const {id} = req.params
        const {nombre_completo, ci,imagen} = req.body
        await Users.update({
            nombre_completo,
            ci,
            imagen
        },{where:{id}})
        res.json({
            error: null,
            msg: "Perfil actualizado exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.ActualizarInfoCorreo = async(req, res)=>{
    try {
        const {id} = req.params
        const {password} = req.body
        const salt = await bcrypt.genSalt(10)
        const pass = await bcrypt.hash(password,salt)
        await Users.update({
            password: pass,
        },{where:{id}})
        res.json({
            error: null,
            msg: "Contraseña actualizada exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.ActualizarEstadoUsuario = async(req, res)=>{
    try {
        const {id} = req.params
        const {estado, rol, turnoId} = req.body

        await Users.update({
            estado,
            rol,
            turnoId
        },{where:{id}})
        res.json({
            error: null,
            msg: "Estado actualizado exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}