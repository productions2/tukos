const {TiposHabitacion} = require("../models/tipoHabitacion.model")

exports.ListarTiposHabitacion = async (req,res)=>{
    const tiposHabitaciones =  await TiposHabitacion.findAll()
    res.json(tiposHabitaciones)
}

exports.CrearTiposHabitacion = async(req, res)=>{
    const {nombre, descripcion} = req.body
    try {
        await TiposHabitacion.create({
            nombre,
            descripcion,
            fecha_registro: Date.now()
        })
        res.json({
            error: null,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BuscarTiposHabitacion = async(req, res)=>{
    const {nombre}= req.params
    const respuesta = await TiposHabitacion.findOne({where:{nombre}})
    res.json(respuesta)
}
exports.BuscarTiposHabitacionID = async(req, res)=>{
    const {id}= req.params
    const respuesta = await TiposHabitacion.findOne({where:{id}})
    res.json(respuesta)
}
exports.EditarTiposHabitacion = async(req, res)=>{
    const {id} = req.params
    const {name, descripcion} = req.body
    await db.client.query("UPDATE tipos_habitacion SET name = $1, descripcion= $2 WHERE id= $3",[name,descripcion,id],(err,result)=>{
        if(err)res.json({"err": "Error al editar"})
        if(result.rowCount<1)res.json({"err":`No se ha encontrado el tipo de habitacion ${id} `})
        else res.json({"msg": `Tipo de habitacion ${id} actualizado correctamente`})
    })
}
exports.BorrarTiposHabitacion = async(req, res)=>{
    const {id} = req.params
    await db.client.query("DELETE FROM tipos_habitacion WHERE id= $1",[id],(err,result)=>{
        if(err)res.json({"err":"Error al eliminar tipo de habitacion"})
        if(result.rowCount<1)res.json({"err": `No se ha encontrado el tipo de habitacion ${id}`})
        else res.json({"msg": `El tipo de habitacion ${id} se ha eliminado correctamente`})
    })
}
