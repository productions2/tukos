const {Transaccion} = require("../models/transaccion.model")

exports.ListarTransaccion = async(req, res)=>{
    const transaccion = await Transaccion.findAll()
    res.json(transaccion)
}

exports.CrearTransaccion = async (req, res)=>{
    const {monto, tipo, descripcion} = req.body
    try {
        const transaccion = await Transaccion.create({
            monto,tipo,descripcion,fecha_registro:Date.now()
        })
        res.json({
            error: null,
            transaccion,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}