const {Tareas} = require("../models/tareas.usuario.model")

exports.ListarTareas = async (req, res)=>{
    const tareas = await Tareas.findAll()
    res.json(tareas)
}

exports.BuscarTareas = async (req, res)=>{
    const {userId, fecha_registro,estado} = req.params
    try {
        const tareas = await Tareas.findAll({where:{userId,fecha_registro,estado}})
        res.json(tareas)
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.CrearTarea =  async (req, res)=>{
    const {descripcion,estado,userId,controles,llaves,tanque1,cajaId,tanque2,tanque3} = req.body
    try {
        const tarea = await Tareas.create({
            descripcion,
            estado,
            fecha_registro: Date.now(),
            userId,
            controles,
            llaves,
            tanque1,
            cajaId,
            tanque2,
            tanque3
        });
        res.json({
        error: null,
        tarea,
        msg: "Registro exitoso"
        });
    } catch (error) {
        res.status(500).json({
        error: true,
        msg: "Error en el registro"
        });
    }
}

exports.ActualizarTarea = async(req, res)=>{

    const {id} = req.params
    const {estado} = req.body
    try {
        await Tareas.update({estado},{where:{id}})
        res.json({msg:"Tarea dada de baja"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.UltimaNovedad = async (req, res) => {
    const tareas = await Tareas.findOne({ order: [['id', 'DESC']] });
    res.json(tareas);
}


