const {Paises} = require("../models/pais.model")

exports.ListarPaises = async (req,res)=>{
    try {
        const paises = await Paises.findAll()
        res.json(paises)    
    } catch (error) {
        res.status(500).json({
            error: true,
            msg: error.message})
    }
}

exports.CrearPais = async(req,res)=>{
    try {
        const { pais, abreviado, img} = req.body
        if(!pais || !abreviado || !img){
            res.json({
                error: true,
                msg: "Rellene todos los campos"})
        }else{
            const newPais = await Paises.create({
                pais,
                abreviado,
                img
            })
            res.json({
                error: null,
                msg: "Registro exitoso"
            })
        }    
    } catch (error) {
        res.status(500).json({
            error: true,
            msg: error.message})
    }    
}
exports.BuscarPais = async(req,res)=>{
    try {
        const {abreviado} = req.params
        const response = await Paises.findOne({where:{abreviado}})
        if(!response){
            res.json({
                error: true,
                msg: "El pais no esta registrado"})
        }else{
            res.json(response)
        }
    } catch (error) {
        res.status(500).json({
            error: true,
            msg: error.message})
    }
}
exports.EditarPais = async(req,res)=>{
    const {id} = req.params
    const {pais, abreviado, img} = req.body
    await db.client.query("UPDATE pais SET pais = $1, abreviado = $2, img=$3 WHERE id = $4",[pais,abreviado,img,id],(err,result)=>{
        //if(err)console.log(err)
        if(err)res.json({"err": "Error al editar pais"})
        if(result.rowCount<1)res.json({"err": `No se ha encontrado el pais ${id}`})
        else res.json({"msg": `Pais con id ${id} actualizado correctamente`})
    })
}
exports.BorrarPais = async(req,res)=>{
    const {id} = req.params
    await db.client.query("DELETE FROM pais WHERE id = $1",[id],(err,result)=>{
        if(err)res.json({"err": "Error al eliminar pais"})
        if(result.rowCount<1)res.json({"err": `No se ha encontrado el pais : ${id}`})
        else res.json({"msg": `Pais con id : ${id} eliminado correctamente`})
    })
}