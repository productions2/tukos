const {Personas} = require("../models/persona.model")

exports.ListarPersona = async (req, res)=>{
        try {
                const personas =  await Personas.findAll({order:[['id','DESC']]})
                res.json(personas)
        } catch (error) {
                res.status(500).json({
                        error: true,
                        msg: error.message})
        }
}

exports.CrearPersona = async(req, res)=>{
        try {
                const { nacionalidad,ci_dni,pasaporte,extension,paisId,nombres,apellidoP,apellidoM, genero,zona,
                        direccion, profesion, contacto, fecha_nacimiento, estado_civil, foto, estado}= req.body
                await Personas.create({
                        nacionalidad,
                        ci_dni,
                        nombres,
                        apellidoP,
                        apellidoM,
                        genero,
                        zona,
                        direccion,
                        profesion,
                        contacto,
                        fecha_nacimiento,
                        estado_civil,
                        estado,
                        foto,
                        fecha_registro: Date.now(),
                        paisId
                })
                res.json({
                        error: null,
                        msg: "Registro exitoso"
                })
        } catch (error) {
                res.json({
                        error: true,
                        msg: error.message})
        }
        
}

exports.BuscarPersonaCi = async(req, res)=>{
        const {ci_dni} = req.params
        const persona =  await Personas.findOne({where:{ci_dni}})
        if(persona) res.json({error: null, persona})
        else res.json({error: true, msg: "Persona no encontrada"})
}
exports.BuscarPersona = async(req, res)=>{
        const {ci_dni} = req.params
        const persona =  await Personas.findOne({where:{id}})
        if(persona) res.json({error: null, persona})
        else res.json({error: true, msg: "Persona no encontrada"})
}
exports.ActualizarPersona = async(req, res)=>{
        const id = req.params.id
        const { nacionalidad,ci_dni,pasaporte,extension,pais_id,nombres,apellidoP,apellidoM, genero,zona,
                direccion, profesion, telefono, celular, fecha_nacimiento, estado_civil, foto, estado}= req.body
        try {
                await Personas.update({
                        nacionalidad,
                        ci_dni,
                        pais_id,
                        nombres,
                        apellidoM,
                        apellidoP,
                        genero,
                        zona,
                        direccion,
                        profesion,
                        telefono,
                        celular,
                        fecha_nacimiento,
                        estado_civil,
                        foto,
                        estado
                },{where:{id}})

                res.json({
                        error: null,
                        msg: "Persona actualizada exitosamente"})
        } catch (error) {
                res.json({
                        error: true,
                        msg: error.message})
        }
}
exports.EliminarPersona = async(req, res)=>{
        const id = req.params.id
        await db.client.query("DELETE FROM personas WHERE id = $1", [id],(err,result)=>{
                if(err) res.json({"err": `Error al eliminar persona con id: ${id}`})
                if(result.rowCount<1)res.json({"err": `No se ha encontrado la persona ${id}`})
                else res.json({"msg":`Persona con id: ${id} eliminada correctamente`})
        })
}