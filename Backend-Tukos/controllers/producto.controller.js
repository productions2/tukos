const {Productos} = require("../models/producto.model")

exports.ListarProducto = async (req, res)=>{
    const productos = await Productos.findAll()
    res.json(productos)
}

exports.CrearProductos = async (req, res)=>{
    const { nombre, descripcion, cantidad, precio,precio_descuento, imagen} = req.body
    try {
        const producto = await Productos.create({
            nombre,
            descripcion,
            cantidad,
            precio,
            precio_descuento,
            imagen,
            fecha_registro : Date.now()
        })
        res.json({
            error: null,
            producto,
            msg: "Producto Agregado"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.BuscarProducto = async (req, res)=>{
    const {id} = req.params
    const producto = await Productos.findOne({where:{id}})
    res.json(producto)
}

exports.ActualizarProducto = async(req, res) =>{
    const {id} = req.params
    const { nombre, descripcion, cantidad, precio, imagen} = req.body
    try {
        await Productos.update({nombre, descripcion, cantidad, precio, imagen},{where:{id}})
        res.json({
            error: null,
            msg: "Producto actualizado"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
