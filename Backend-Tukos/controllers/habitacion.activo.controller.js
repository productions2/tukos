const {HabitacionActivos} = require("../models/habitacionActivos.model")

exports.ListarActivosHabitacion = async (req, res)=>{
    const activos = await HabitacionActivos.findAll()
    res.json(activos)
}

exports.CrearHabitacionActivo = async (req,res)=>{
    const {estado, activoId, habitacionId} = req.body
    try {
        await HabitacionActivos.create({
            estado,
            activoId,
            fecha_registro: Date.now(),
            habitacionId
        })
        res.json({
            error: null,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.BuscarActivoIdHab = async(req, res)=>{
    const {habitacionId} = req.params
    try {
        const activos = await HabitacionActivos.findAll({where:{habitacionId}})
        res.json(activos)
    } catch (error) {
        return
    }

}

exports.BorrarActivoHab = async(req, res)=>{
    const {activoId} = req.params
    try {
        await HabitacionActivos.destroy({where:{activoId}})
        res.json({
            error: null,
            msg: "Eliminado exitosamente"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}