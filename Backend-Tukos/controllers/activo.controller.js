const {Activos} = require("../models/activo.model")

exports.ListarActivos = async (req,res)=>{
    const activos = await Activos.findAll({order:[['id','ASC']]})
    res.json(activos)   
}
exports.BuscarActivo = async(req, res)=>{
    const {id} = req.params
    const activo = await Activos.findOne({where:{id}})
    if(!activo){ 
        return res.json({
        error: true,
        msg: "Activo no encontrado"
        })
    }
    else res.json(activo)
}
exports.CrearActivo = async(req, res)=>{
    const {nombre, codigo_activo, descripcion, marca, estado, imagen} = req.body
    try {
        const activo = await Activos.create({
            nombre,
            codigo_activo,
            descripcion,
            marca,
            estado,
            imagen,
            fecha_registro: Date.now()
        })
        res.json({
            error: null,
            activo,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.EditarActivo = async(req, res)=>{
    const {nombre, descripcion, marca, estado, imagen} = req.body
    const {id} = req.params
    try {
        await Activos.update({
            nombre,
            descripcion,
            marca,
            estado,
            imagen
        },{where:{id}})
        res.json({
            error: null,
            msg: "Activo actualizado exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BorrarActivo = async(req, res)=>{
    const {id} = req.params
    await db.client.query("DELETE FROM activos WHERE id = $1",[id],(err, result)=>{
        if(err)res.json({"err": "Error al eliminar"})
        if(result.rowCount<1) res.json({"err": `No se ha encontrado el activo: ${id}`})
        else res.json({"msg": `Activo con id ${id} eliminado satisfactoriamente`})
    })
}
