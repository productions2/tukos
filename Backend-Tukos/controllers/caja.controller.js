const {Cajas} = require("../models/cajas.model")
const { Op } = require('sequelize');

exports.ListarCajas = async(req, res)=>{
    const cajas = await Cajas.findAll()
    res.json(cajas)
}

exports.CrearCaja = async(req, res)=>{
    try {
        const {monto_inicial, monto_reserva, monto_cierre, estado,userId} = req.body
        const caja = await Cajas.create({
            monto_inicial,
            monto_reserva,
            monto_cierre,
            estado,
            fecha_registro : Date.now(),
            userId
        })
        res.json({
            error: null,
            id_caja: caja.id,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
    
}
exports.BuscarCaja = async(req, res)=>{
    const {userId} = req.params
    const id = await Cajas.max("id",{where:{userId}} )
    const caja = await Cajas.findOne({where:{id}})
    res.json(caja)
}

exports.BuscarCajaInicio = async(req, res)=>{
    const id = await Cajas.max("id",{where:{estado:false}} )
    const caja = await Cajas.findOne({where:{id}})
    res.json(caja)
}
exports.ActualizarCaja = async(req,res)=>{
    try {
        const {id} = req.params
        const {monto_reserva,monto_cierre,estado,userId} = req.body
        await Cajas.update({
            monto_reserva,
            monto_cierre,
            estado,
            userId
        },{where:{id}})
        res.json({
            error: null,
            msg: "Cierre de caja exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.UltimaCaja = async (req, res) => {
    const caja = await Cajas.findOne({ order: [['id', 'DESC']] });
    res.json(caja);
  }
  

  exports.ReporteCajas = async (req, res) => {
    const { userId } = req.params;
    const { fecha_inicio, fecha_fin } = req.query;

    const cajas = await Cajas.findAll({
    where: {
        userId,
        fecha_registro: {
        [Op.between]: [new Date(fecha_inicio), new Date(fecha_fin)]
        }
    }
    });

    res.json(cajas);
};