const {ReportePersonal} = require("../models/reporte.personal.model")
const {Users} = require("../models/user.model")

exports.ListarReporte = async(req,res)=>{
    const reporte = await ReportePersonal.findAll()
    res.json(reporte)
}

exports.BuscarReporte = async(req, res)=>{
    const {fecha, userId} = req.params 
    const reporte = await ReportePersonal.findOne({where:{fecha,userId}})
    res.json(reporte)
}

exports.CrearReporte = async (req, res) =>{
    const {userId} = req.body
    const date = new Date()
    const hora = date.getHours()
    const minut = date.getMinutes()
    const seconds = date.getSeconds()

    const hora_i = hora+":"+minut+":"+seconds
    //const seconds = date.getSeconds
    try {
        const reporte = await ReportePersonal.create({
            hora_ingreso: hora_i,
            hora_salida:null,
            userId,
            fecha: Date.now()
        })
        res.json({
            error: null,
            reporte,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.ActualizarReporte = async(req, res)=>{
    try {
        const {id} = req.params
        const date = new Date()
        const hora = date.getHours()
        var minut = date.getMinutes()
        if(minut.toString().length==1){
            minut = "0"+minut
        }
        const seconds = date.getSeconds()
        const hora_salida = hora+":"+minut+":"+seconds
        console.log(id)
        const reporte = await ReportePersonal.update({hora_salida},{where:{id}})
        res.json(reporte)
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}