const {Caracteristicas} = require("../models/caracteristicasHabitacion.model")

exports.ListarCaracteristicas = async (req, res)=>{
    const caracteristicas = await Caracteristicas.findAll()
    res.json(caracteristicas)
}

exports.CrearCaracteristica = async(req, res)=>{
    try {
        const {caracteristica} = req.body
        await Caracteristicas.create({
            caracteristica,
            fecha_registro: Date.now()
        })
        res.json({
            error: null,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}