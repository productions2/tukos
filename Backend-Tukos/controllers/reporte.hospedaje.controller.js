const {RegistroHospedaje} = require("../models/registroHospedaje.model")

exports.ListarReporteHospedaje = async (req, res)=>{
    const reporte = await RegistroHospedaje.findAll()
    res.json(reporte)
}

exports.CrearReporteHospedaje = async (req,res)=>{
    try {
        const {dni_huespedes, procedencia,cant_mayores, cant_menores, destino,trabajo,num_noches,
        pago_noche,pago_total,fecha_ingreso,fecha_salida,estado, personaId,habitacionId} = req.body
        
        const reporte = await RegistroHospedaje.create({
            dni_huespedes,
            procedencia,
            cant_mayores,
            cant_menores,
            destino,
            trabajo,
            num_noches,
            pago_noche,
            pago_total,
            fecha_ingreso,
            fecha_salida,
            estado,
            fecha_registro: Date.now(),
            personaId,
            habitacionId
        })
        res.json({
            error: null,
            resonse: reporte,
            msg: "Registro exitoso"
    })

    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}