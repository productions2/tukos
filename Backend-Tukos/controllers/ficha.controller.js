const {Fichas} = require("../models/reporte.fichas.model")

exports.ListarFichas = async(req, res)=>{
    const ficha = await Fichas.findAll()
    res.json(ficha)
}
exports.BuscarFicha = async(req, res)=>{
    const {userId} = req.params
    const fichas = await Fichas.findAll({where:{userId}})
    res.json(fichas)
}
exports.CrearFicha = async(req,res)=>{
    const {tipo, asunto,descripcion,adminId,userId,monto} = req.body
    

    try {
        const ficha = await Fichas.create({
            tipo,
            asunto,
            descripcion,
            fecha_registro: Date.now(),
            adminId,
            monto,
            userId
        })
        res.json({
            error: null,
            ficha,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}