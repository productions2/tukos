const {Roles} =  require("../models/rol.model")

exports.ListarRoles = async(req, res)=>{
    const roles  = await Roles.findAll()
    res.json(roles)
}

exports.CrearRol = async(req, res)=>{
    try {
        const {rol, descripcion, estado} = req.body
        const newRol = await Roles.create({
            rol,
            descripcion,
            estado
        })
        res.json({
            error: null,
            rol: newRol,
            msg: "Registro exitoso"
        })
        
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.ActualizarEstado  = async(req, res )=>{
    try {
        const {id} = req.params
        const {estado} = req.body
        await Roles.update({
            estado,
        },{where:{id}})
        res.json({
            error: null,
            msg: "Rol actualizado"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}