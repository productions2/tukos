const {DetalleCajas} = require("../models/detalleCaja")
const {Cajas} = require("../models/cajas.model")
const { Op } = require('sequelize');

exports.ListarDetalleCaja = async(req, res)=>{
    const listaDetalle = await DetalleCajas.findAll({
        order: [['id', 'DESC']]
    })
    res.json(listaDetalle)
}

exports.CrearDetalleCaja = async(req, res) =>{
    try {
        const {monto, tipo, fichaId, cajaId,transaccionId, descripcion} = req.body
        await DetalleCajas.create({
            monto,
            tipo,
            fecha_registro: Date.now(),
            fichaId,
            cajaId,
            transaccionId,
            descripcion
        })
        res.json({
            error: null,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
} 

exports.ReporteCajaDetalles = async (req, res) => {
    const { userId } = req.params;
    const { fecha_inicio, fecha_fin } = req.query;

    const cajas = await Cajas.findAll({
    where: {
        userId,
        fecha_registro: {
        [Op.between]: [new Date(fecha_inicio), new Date(fecha_fin)]
        }
    },
    include: [{
        model: DetalleCajas,
        as: 'detalles'
    }]
    });

    res.json(cajas);
};
  