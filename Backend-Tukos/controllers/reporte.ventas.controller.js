const {Ventas} = require("../models/venta.model")

exports.ListarVentas = async(req, res)=>{
    const ventas = await Ventas.findAll()
    res.json(ventas)
}

exports.CrearVenta = async (req, res)=>{
    const {precio_total, descuento_total} = req.body
    try {
        const venta = await Ventas.create({
            precio_total,
            descuento_total,
            fecha_registro: Date.now()
        })
        res.json({
            error: null,
            venta,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}