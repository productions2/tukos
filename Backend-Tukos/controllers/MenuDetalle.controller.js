const {MenuReserva} =  require("../models/menuReserva.model")

exports.ListarMenuDetalle = async(req, res)=>{
    const menuDetalle = await MenuReserva.findAll()
    res.json(menuDetalle)
}

exports.CrearMenuDetalle = async (req, res)=>{
    const {cantidad, precio, descuento,fichaId,menuId,productoId,VentaId}= req.body
    try {
        const detalle = await MenuReserva.create({
            cantidad, precio,descuento,fecha_registro:Date.now(),
            fichaId,menuId,productoId,VentaId
        })
        res.json({
            error: null,
            detalle,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}