const {Turnos} = require("../models/turnos.model")

exports.ListarTurnos = async(req, res)=>{
    const turnos = await Turnos.findAll()
    res.json(turnos)
}
exports.CrearTurno = async(req, res)=>{
    try {
        const {turno, hora_ingreso, hora_salida} = req.body
        const newTurno = await Turnos.create({
            turno,
            hora_ingreso,
            hora_salida
        })
        res.json({
            error: null,
            turno: newTurno,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.BuscarTurno = async (req, res)=>{
    try {
        const {id} = req.params
        const turno = await Turnos.findOne({where:{id}})
        res.json(turno)
    } catch (error) {
        
    }
}