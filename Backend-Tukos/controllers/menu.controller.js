const {MenuHotel} = require("../models/menuHotel")

exports.ListarMenu = async (req, res)=>{
    const menu = await MenuHotel.findAll()
    res.json(menu)
}

exports.CrearMenu = async(req, res)=>{
    const {producto, descripcion, precio_cu, precio_descuento, estado, imagen} = req.body
    try {
        const menu = await MenuHotel.create({
            producto,descripcion,precio_cu,precio_descuento, estado, imagen, fecha_registro:Date.now()
        })
        res.json({
            error: null,
            menu,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BuscarMenu = async(req, res)=>{
    const {id} = req.params
    const menu =  await MenuHotel.findOne({where:{id}})
    res.json(menu)
}
exports.actualizarMenu = async(req, res) =>{
    const {id} = req.params
    const {producto,descripcion,precio_cu,precio_descuento} = req.body
    try {
        await MenuHotel.update({
            producto,descripcion,precio_cu,precio_descuento
        },{where:{id}})
        res.json({
            error: null,
            msg: "Actualizacion exitosa"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.EstadoMenu = async(req, res)=>{
    const {id} = req.params
    const {estado} = req.body
    try {
        await MenuHotel.update({estado},{where:{id}})
        res.json({
            error: null,
            msg: "Actualizacion exitosa"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BorrarMenu = async(req, res)=>{
    const {id} = req.params
    await db.client.query("DELETE FROM menu_hotel WHERE id = $1", [id],(err,result)=>{
        if(err)res.json({"err": "Error al eliminar menu"})
        if(result.rowCount<1)res.json({"err":`No se ha encontrado el menu ${id} `})
        else res.json({"msg": `Menu con id : ${id} eliminado correctamente`})
    })
}