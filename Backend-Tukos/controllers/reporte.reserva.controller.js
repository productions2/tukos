const {RegistroReserva} = require("../models/registroreserva.model")
const {Personas} = require("../models/persona.model")
const {Habitaciones} = require("../models/habitacion.model")

exports.ListarReporte = async (req, res) => {
    try {
        const registroReserva = await RegistroReserva.findAll({
        include: [
            { model: Personas },
            { model: Habitaciones }
        ]
        });

        res.json(registroReserva);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Error al obtener el reporte' });
    }
};

exports.CrearReporte = async(req, res)=>{
    try {
        const { CiTitular, CiAcompañantes, userId, procedencia, destino,cantidad_personas,trabajo,
            pago,num_noches,saldo,estado,tipo,personaId,habitacionId,fechaingreso,fechasalida} = req.body
        const reporte = await RegistroReserva.create({
           CiTitular,
           CiAcompañantes,
           procedencia,
           destino,
           cantidad_personas,
           pago,
           trabajo,
           num_noches,
           saldo,
           estado,
           tipo,
           fecha_registro: Date.now(),
           ultima_actualizacion: Date.now(),
           userId,
           personaId,
           habitacionId,
           fechaingreso,
           fechasalida
        })
        res.json({
            error: null,
            response: reporte,
            msg: "Registro exitoso"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
    
}
exports.BuscarReporte = async(req, res)=>{
    const {habitacionId, estado}= req.params
    const reporte = await RegistroReserva.findOne({where:{habitacionId, estado}})
    res.json(reporte)
}
exports.EditarReporte = async(req, res)=>{
    const {id} = req.params
    const {estado,saldo,pago} = req.body
    try {
        await RegistroReserva.update({estado,saldo,pago},{where:{id}})
        res.json({
            error: null,
            msg: "Actualizacion exitosa"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BorrarReporte = async(req, res)=>{
    const {id}= req.params
    await db.client.query("DELETE FROM registro_reserva WHERE id=$1",[id],(err,result)=>{
        if(err)res.json({"err": "Error al eliminar reporte de reservacion"})
        if(result.rowCount<1)res.json({"err": `No se ha encontrado el reporte de reservacion ${id}`})
        else res.json({"msg": `El reporte de reservacion ${id} se elimino correctamente`})
    })
}
