const {Boletas} = require("../models/reporte.boletas.model")

exports.ListarBoletas = async(req,res)=>{
    const boletas = await Boletas.findAll()
    res.json(boletas)
}

exports.BuscarBoleta = async(req, res)=>{
    const {mes, año, userId} = req.body 

    const boleta = await Boletas.findOne({where:{mes,año,userId}})
    res.json(boleta)
}

exports.CrearBoleta = async(req,res)=>{
    const {mes,año,dias,ingresos,descuentos,total,estado,userId} = req.body

    const exist = await Boletas.findOne({where:{mes,año}})
    console.log(exist)
    if(!exist){
        try {
            const boleta = await Boletas.create({
                mes,
                año,
                dias,
                ingresos,
                descuentos,
                total,
                estado,
                userId
            })
            res.json({
                error: null,
                boleta,
                msg: "Registro exitoso"})
        } catch (error) {
            res.json({
                error: true,
                msg: error.message})
        }
    }else{
        res.json({
            error: true,
            msg: "Ya existe un registro con ese mes y año"})
    }
    
}