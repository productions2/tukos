const { Op } = require("sequelize")
const { sequelize } = require("../connectionDB")
const {Habitaciones} = require("../models/habitacion.model")

exports.ListarHabitaciones = async (req, res)=>{
    const habitaciones =  await Habitaciones.findAll()
    res.json(habitaciones)
}

exports.ListarHabitacionesLibres = async (req, res) => {
    const habitaciones = await Habitaciones.findAll({
        where: {
        estado: 'disponible'
        }
    })
    res.json(habitaciones)
}

exports.BuscarHabitacion = async(req, res)=>{
    const {id} = req.params
    const habitacion = await Habitaciones.findOne({where:{id}})
    if(!habitacion){ 
        return res.json({
        error: true,
        msg: "Habitacion no encontrada"
        })
    }
    else res.json(habitacion)
}
exports.BuscarHabitacionFiltros = async(req, res)=>{
    const {tipoId, codigo_habitacion, estado} = req.body
    console.log(req.body)
    var habitacion 
    if(tipoId) habitacion = await Habitaciones.findAll({where:{tipoId}})
    if(codigo_habitacion) habitacion = await Habitaciones.findAll({where:{codigo_habitacion}})
    if(estado) habitacion = await Habitaciones.findAll({where:{estado}})

    if(!habitacion){ 
        return res.json({
        error: true,
        msg: "Habitacion no encontrada"
        })
    }
    else res.json(habitacion)
}
exports.BuscarMaxID = async(req,res)=>{
    try {
        const result = await Habitaciones.max('id')
        if(!result)res.json({max: 0})
        else res.json({max: result})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
    
}

exports.CrearHabitacion = async (req, res)=>{
    const {codigo_habitacion, descripcion,caracteristicas,imagenes,precio_noche,estado,tipoId,piso,user_id} = req.body
    try {
        console.log(req.body)
        await Habitaciones.create({
            codigo_habitacion,
            descripcion,
            caracteristicas,
            imagenes,
            precio_noche,
            estado,
            fecha_registro: Date.now(),
            tipoId,
            piso,
            user_id,
        })
        res.json({
            error: null,
            msg: "Registro exitoso"
        })
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}

exports.EditarHabitacion = async(req, res)=>{
    const {id} = req.params
    const {precio_noche,descripcion,imagenes,estado} = req.body
    try {
        await Habitaciones.update({precio_noche,
        descripcion,imagenes,estado},{where:{id}})
        res.json({
            error: null,
            msg: "Habitacion actualizado exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.EditarEstado = async(req, res)=>{
    const {id} =  req.params
    const {estado} = req.body
    try {
        await Habitaciones.update({estado},{where:{id}})
        res.json({
            error: null,
            msg: "Habitacion actualizado exitosamente"})
    } catch (error) {
        res.json({
            error: true,
            msg: error.message})
    }
}
exports.BorrarHabitacion = async(req, res) =>{
    const {id} = req.params
    await db.client.query("DELETE FROM habitacion WHERE id=$1",[id],(err,result)=>{
        if(err) res.json({"err": `Error al eliminar habitacion ${id}`})
        if(result.rowCount<1) res.json({"err": `No se ha encontrado la habitacion: ${id}`})
        else res.json({"msg": `Habitacion con id ${id} eliminada satisfactoriamente`})
    })
}

exports.CantidadHabitaciones = async (req, res)=>{
    const habitaciones =  await Habitaciones.count()
    res.json(habitaciones)
}