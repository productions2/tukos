const app = require("./app");
const { sequelize } = require("./connectionDB");
const Roles = require("./models/rol.model");
const Productos = require("./models/producto.model");
const Menu = require("./models/menuHotel");
const Ventas = require("./models/venta.model");
const transaccion = require("./models/transaccion.model");
const config = require("./configurations/configurations");
//sequelize.sync({force:true})
sequelize.authenticate();

const port = config.port;

app.listen(port, "0.0.0.0", () => {
  console.log(`Server started on port ${port}`);
});
