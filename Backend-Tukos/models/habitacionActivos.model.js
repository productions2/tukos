const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const HabitacionActivos = sequelize.define(
  "habitacionActivos",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    estado: {
      type: DataTypes.ENUM,
      values: ["activo", "baja"],
      defaultValue: "baja",
      allowNull: false,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { HabitacionActivos };
