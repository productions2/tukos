const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { HabitacionActivos } = require("./habitacionActivos.model");
const { RegistroReserva } = require("./registroreserva.model");
const { RegistroHospedaje } = require("./registroHospedaje.model");

const Habitaciones = sequelize.define(
  "habitaciones",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    codigo_habitacion: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
    },
    caracteristicas: {
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    imagenes: {
      type: DataTypes.ARRAY(DataTypes.TEXT("long")),
    },
    precio_noche: {
      type: DataTypes.DOUBLE,
    },
    estado: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    piso: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

Habitaciones.hasMany(HabitacionActivos, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});

HabitacionActivos.belongsTo(Habitaciones, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});

Habitaciones.hasMany(RegistroReserva, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});

RegistroReserva.belongsTo(Habitaciones, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});
Habitaciones.hasMany(RegistroHospedaje, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});

RegistroHospedaje.belongsTo(Habitaciones, {
  foreignKey: {
    name: "habitacionId",
    allowNull: false,
  },
});
module.exports = { Habitaciones };
