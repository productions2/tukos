const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { MenuReserva } = require("./menuReserva.model");

const MenuHotel = sequelize.define(
  "menuHotels",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    producto: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    precio_cu: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    precio_descuento: {
      type: DataTypes.DOUBLE,
    },
    estado: {
      type: DataTypes.BOOLEAN,
    },
    imagen: {
      type: DataTypes.TEXT("long"),
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { MenuHotel };
