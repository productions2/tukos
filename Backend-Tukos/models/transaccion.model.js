const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { DetalleCajas } = require("../models/detalleCaja");

const Transaccion = sequelize.define(
  "Transaccion",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    monto: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    tipo: {
      type: DataTypes.STRING,
    },
    descripcion: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);
Transaccion.hasMany(DetalleCajas, {
  foreignKey: {
    name: "transaccionId",
  },
});
DetalleCajas.belongsTo(Transaccion, {
  foreignKey: {
    name: "transaccionId",
  },
});
module.exports = { Transaccion };
