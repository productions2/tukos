const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { Users } = require("./user.model");

const Turnos = sequelize.define(
  "turnos",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    turno: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    hora_ingreso: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    hora_salida: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);
Turnos.hasMany(Users, {
  foreignKey: {
    name: "turnoId",
  },
});
Users.belongsTo(Turnos, {
  foreignKey: {
    name: "turnoId",
  },
});
module.exports = { Turnos };
