const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const Tareas = sequelize.define(
  "tareas",
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    estado: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    fecha_registro: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    controles: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    llaves: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    tanque1: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    cajaId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    tanque2: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    tanque3: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { Tareas };
