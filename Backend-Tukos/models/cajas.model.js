const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { DetalleCajas } = require("./detalleCaja");

const Cajas = sequelize.define(
  "cajas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    monto_inicial: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    monto_reserva: {
      type: DataTypes.DOUBLE,
    },
    monto_cierre: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    estado: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

Cajas.hasMany(DetalleCajas, {
  foreignKey: {
    name: "cajaId",
    allowNull: false,
  },
  as: "detalles",
});

DetalleCajas.belongsTo(Cajas, {
  foreignKey: {
    name: "cajaId",
    allowNull: false,
  },
});

module.exports = { Cajas };
