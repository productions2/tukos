const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { RegistroReserva } = require("./registroreserva.model");
const { RegistroHospedaje } = require("./registroHospedaje.model");

const Personas = sequelize.define(
  "personas",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nacionalidad: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ci_dni: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    apellidoP: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    apellidoM: {
      type: DataTypes.STRING,
    },
    genero: {
      type: DataTypes.ENUM,
      values: ["Femenino", "Masculino"],
      allowNull: false,
    },
    zona: {
      type: DataTypes.STRING,
    },
    direccion: {
      type: DataTypes.STRING,
    },
    profesion: {
      type: DataTypes.STRING,
    },
    contacto: {
      type: DataTypes.STRING,
    },
    fecha_nacimiento: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    estado_civil: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    estado: {
      type: DataTypes.ENUM,
      values: ["activo", "baja"],
      defaultValue: "baja",
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    foto: {
      type: DataTypes.TEXT,
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
  },
  {
    timestamps: false,
  }
);

Personas.hasMany(RegistroReserva, {
  foreignKey: {
    name: "personaId",
    allowNull: false,
  },
});
RegistroReserva.belongsTo(Personas, {
  foreignKey: {
    name: "personaId",
    allowNull: false,
  },
});
Personas.hasMany(RegistroHospedaje, {
  foreignKey: {
    name: "personaId",
    allowNull: false,
  },
});
RegistroHospedaje.belongsTo(Personas, {
  foreignKey: {
    name: "personaId",
    allowNull: false,
  },
});
module.exports = { Personas };
