const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { HabitacionActivos } = require("./habitacionActivos.model");

const Activos = sequelize.define(
  "activos",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    codigo_activo: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    marca: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    estado: {
      type: DataTypes.ENUM,
      values: ["disponible", "ocupado", "mantenimiento"],
      defaultValue: "disponible",
      allowNull: false,
    },
    imagen: {
      type: DataTypes.TEXT("long"),
      allowNull: false,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

Activos.hasMany(HabitacionActivos, {
  foreignKey: {
    name: "activoId",
    allowNull: false,
  },
});

HabitacionActivos.belongsTo(Activos, {
  foreignKey: {
    name: "activoId",
    allowNull: false,
  },
});

module.exports = { Activos };
