const { sequelize } = require("../connectionDB");
const { DataTypes } = require("sequelize");
const Caracteristicas = sequelize.define(
  "caracteristicas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    caracteristica: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { Caracteristicas };
