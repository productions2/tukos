const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const ReportePersonal = sequelize.define(
  "reportesPersonal",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    hora_ingreso: {
      type: DataTypes.TIME,
    },
    hora_salida: {
      type: DataTypes.TIME,
    },
    fecha: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { ReportePersonal };
