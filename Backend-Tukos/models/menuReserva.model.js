const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const MenuReserva = sequelize.define(
  "menuReservas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    cantidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
        isNumeric: true,
      },
    },
    precio: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    descuento: {
      type: DataTypes.DOUBLE,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    menuId: {
      type: DataTypes.INTEGER,
    },
    productoId: {
      type: DataTypes.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);
module.exports = { MenuReserva };
