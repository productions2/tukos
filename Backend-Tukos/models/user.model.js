const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { RegistroReserva } = require("./registroreserva.model");
const { Cajas } = require("./cajas.model");
const { ReportePersonal } = require("./reporte.personal.model");

const { Boletas } = require("./reporte.boletas.model");
const { Fichas } = require("./reporte.fichas.model");
const { Tareas } = require("./tareas.usuario.model");
const { Habitaciones } = require("./habitacion.model");
const Users = sequelize.define(
  "users",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      required: true,
      validate: {
        isEmail: {
          msg: "Must be a valid email address",
        },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
      required: true,
    },
    nombre_completo: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    ci: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    estado: {
      type: DataTypes.ENUM,
      values: ["Habilitado", "Deshabilitado"],
      defaultValue: "Deshabilitado",
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    rol: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    imagen: {
      type: DataTypes.TEXT("long"),
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    turnoId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: "turnos",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "SET NULL",
    },
  },
  {
    timestamps: false,
  }
);
Users.hasMany(Habitaciones, {
  foreignKey: {
    name: "user_id",
    allowNull: false,
  },
});
Users.hasMany(RegistroReserva, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
RegistroReserva.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

Users.hasMany(Cajas, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
Cajas.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

Users.hasMany(ReportePersonal, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

ReportePersonal.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
Users.hasMany(Boletas, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

Boletas.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
Users.hasMany(Fichas, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

Fichas.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
Users.hasMany(Tareas, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});

Tareas.belongsTo(Users, {
  foreignKey: {
    name: "userId",
    allowNull: false,
  },
});
module.exports = { Users };
