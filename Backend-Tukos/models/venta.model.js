const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { MenuReserva } = require("../models/menuReserva.model");
const { DetalleCajas } = require("./detalleCaja");

const Ventas = sequelize.define(
  "ventas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    precio_total: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    descuento_total: {
      type: DataTypes.DOUBLE,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

Ventas.hasMany(MenuReserva, {
  foreignKey: {
    name: "VentaId",
    allowNull: false,
  },
});

MenuReserva.belongsTo(Ventas, {
  foreignKey: {
    name: "VentaId",
    allowNull: false,
  },
});

Ventas.hasMany(DetalleCajas, {
  foreignKey: {
    name: "ventaId",
  },
});
DetalleCajas.belongsTo(Ventas, {
  foreignKey: {
    name: "ventaId",
  },
});
module.exports = { Ventas };
