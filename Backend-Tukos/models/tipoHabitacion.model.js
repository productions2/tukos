const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { Habitaciones } = require("./habitacion.model");

const TiposHabitacion = sequelize.define(
  "tiposHabitaciones",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

TiposHabitacion.hasMany(Habitaciones, {
  foreignKey: {
    name: "tipoId",
    allowNull: false,
  },
});

Habitaciones.belongsTo(TiposHabitacion, {
  foreignKey: {
    name: "tipoId",
    allowNull: false,
  },
});

module.exports = { TiposHabitacion };
