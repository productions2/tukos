const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { MenuReserva } = require("./menuReserva.model");

const RegistroHospedaje = sequelize.define(
  "registrosHospedaje",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    dni_huespedes: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: false,
    },
    procedencia: {
      type: DataTypes.STRING,
    },
    cant_mayores: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    cant_menores: {
      type: DataTypes.INTEGER,
    },
    destino: {
      type: DataTypes.STRING,
    },
    trabajo: {
      type: DataTypes.BOOLEAN,
    },
    num_noches: {
      type: DataTypes.INTEGER,
    },
    pago_noche: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    pago_total: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    fecha_ingreso: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    fecha_salida: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    estado: {
      type: DataTypes.BOOLEAN,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { RegistroHospedaje };
