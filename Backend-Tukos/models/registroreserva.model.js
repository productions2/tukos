const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { DetalleCajas } = require("./detalleCaja");

const RegistroReserva = sequelize.define(
  "registroReservas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    CiTitular: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    CiAcompañantes: {
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    procedencia: {
      type: DataTypes.STRING,
    },
    destino: {
      type: DataTypes.STRING,
    },
    cantidad_personas: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    trabajo: {
      type: DataTypes.BOOLEAN,
    },
    pago: {
      type: DataTypes.DOUBLE,
    },
    num_noches: {
      type: DataTypes.INTEGER,
    },
    saldo: {
      type: DataTypes.DOUBLE,
    },
    estado: {
      type: DataTypes.BOOLEAN,
    },
    tipo: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    ultima_actualizacion: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    fechaingreso: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    fechasalida: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);
RegistroReserva.hasMany(DetalleCajas, {
  foreignKey: {
    name: "fichaId",
  },
});
DetalleCajas.belongsTo(RegistroReserva, {
  foreignKey: {
    name: "fichaId",
  },
});
module.exports = { RegistroReserva };
