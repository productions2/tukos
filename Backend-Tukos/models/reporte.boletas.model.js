const { DataTypes, JSONB } = require("sequelize");
const { sequelize } = require("../connectionDB");

const Boletas = sequelize.define(
  "boletas",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    mes: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    año: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    dias: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    ingresos: {
      type: DataTypes.ARRAY(JSONB),
      allowNull: false,
    },
    descuentos: {
      type: DataTypes.ARRAY(JSONB),
      allowNull: false,
    },
    total: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    estado: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { Boletas };
