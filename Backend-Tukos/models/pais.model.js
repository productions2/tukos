const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { Personas } = require("./persona.model");

const Paises = sequelize.define(
  "paises",
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    pais: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        notEmpty: true,
      },
    },
    abreviado: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        notEmpty: true,
      },
    },
    img: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);

Paises.hasMany(Personas, {
  foreignKey: {
    name: "paisId",
  },
});

Personas.belongsTo(Paises, {
  foreignKey: {
    name: "paisId",
  },
});

module.exports = { Paises };
