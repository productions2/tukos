const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const AdminCaja = sequelize.define(
  "adminCajas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    cantidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
        isNumeric: true,
      },
    },
    precio: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    descuento: {
      type: DataTypes.BOOLEAN,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);
module.exports = { AdminCaja };
