const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");

const DetalleCajas = sequelize.define(
  "detalleCajas",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    monto: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    tipo: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = { DetalleCajas };
