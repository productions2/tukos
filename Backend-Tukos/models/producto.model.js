const { DataTypes } = require("sequelize");
const { sequelize } = require("../connectionDB");
const { MenuReserva } = require("../models/menuReserva.model");

const Productos = sequelize.define(
  "productos",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    cantidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    precio: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    precio_descuento: {
      type: DataTypes.DOUBLE,
    },
    imagen: {
      type: DataTypes.TEXT("long"),
      allowNull: false,
    },
    fecha_registro: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    timestamps: false,
  }
);
module.exports = { Productos };
