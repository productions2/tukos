const {Router} = require("express")
const pais = require("../controllers/pais.controller")
const router = Router()

router.get("/pais",pais.ListarPaises)
router.get("/pais/:abreviado",pais.BuscarPais)
router.post("/pais",pais.CrearPais)
router.put("/pais/:id",pais.EditarPais)
router.delete("/pais/:id",pais.BorrarPais)

module.exports = router