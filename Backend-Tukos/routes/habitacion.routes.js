const {Router} = require("express")
const habitacion = require("../controllers/habitacion.controller")
const router = Router()

router.get("/habitacion",habitacion.ListarHabitaciones)
router.get("/ListarHabitacionesLibres",habitacion.ListarHabitacionesLibres)
router.get("/habitacion/:id",habitacion.BuscarHabitacion)
router.post("/habitacionFiltros",habitacion.BuscarHabitacionFiltros)
router.get("/habitacionMax",habitacion.BuscarMaxID)
router.post("/habitacion",habitacion.CrearHabitacion)
router.put("/habitacion/:id",habitacion.EditarHabitacion)
router.put("/habitacionEstado/:id",habitacion.EditarEstado)
router.delete("/habitacion/:id",habitacion.BorrarHabitacion)
router.get("/habitacioncount",habitacion.CantidadHabitaciones)

module.exports = router