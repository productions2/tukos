const {Router} = require("express")
const habitacion_activo = require("../controllers/habitacion.activo.controller")
const router = Router()

router.get("/habitacion_activo",habitacion_activo.ListarActivosHabitacion)
router.get("/habitacion_activo/:habitacionId",habitacion_activo.BuscarActivoIdHab)
router.post("/habitacion_activo",habitacion_activo.CrearHabitacionActivo)
router.delete("/habitacion_activo/:activoId",habitacion_activo.BorrarActivoHab)

module.exports = router