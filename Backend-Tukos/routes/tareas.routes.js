const {Router}=require("express")
const tareas = require("../controllers/tareas.controller")

const router = Router()

router.get("/tarea",tareas.ListarTareas)
router.get("/ultimanovedad",tareas.UltimaNovedad)
router.get("/tarea/:userId/:fecha_registro/:estado", tareas.BuscarTareas)
router.post("/tarea", tareas.CrearTarea)
router.put("/tarea/:id", tareas.ActualizarTarea)


module.exports = router