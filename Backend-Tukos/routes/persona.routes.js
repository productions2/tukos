const { Router } = require("express")
const persona = require("../controllers/persona.controller")
const router = Router()

    router.get("/persona",persona.ListarPersona)
    router.get("/persona/:id",persona.BuscarPersona)
    router.get("/personaCi/:ci_dni",persona.BuscarPersonaCi)
    router.post("/persona",persona.CrearPersona)
    router.put("/persona/:id", persona.ActualizarPersona)
    router.delete("/persona/:id",persona.EliminarPersona)


module.exports = router