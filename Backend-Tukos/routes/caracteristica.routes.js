const {Router} = require("express")
const caracteristica = require("../controllers/caracteristicas.controller")

const router = Router()

router.get("/caracteristica",caracteristica.ListarCaracteristicas)
router.post("/caracteristica",caracteristica.CrearCaracteristica)

module.exports = router