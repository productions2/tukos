const {Router} = require("express")
const reporteHospedaje = require("../controllers/reporte.hospedaje.controller")
const router = Router()

router.get("/reporte_hospedaje",reporteHospedaje.ListarReporteHospedaje)
router.post("/reporte_hospedaje", reporteHospedaje.CrearReporteHospedaje)

module.exports = router