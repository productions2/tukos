const {Router} = require ("express")
const reporteReserva = require("../controllers/reporte.reserva.controller")
const router = Router()

router.get("/reporte_reserva", reporteReserva.ListarReporte)
router.get("/reporte_reserva/:habitacionId/:estado",reporteReserva.BuscarReporte)
router.post("/reporte_reserva",reporteReserva.CrearReporte)
router.put("/reporte_reserva/:id",reporteReserva.EditarReporte)
router.delete("/reporte_reserva/:id",reporteReserva.BorrarReporte)

module.exports = router