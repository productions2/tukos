const {Router}  = require("express")
const reporte  = require("../controllers/reporte.persona.controller")

const router = Router()

router.get("/reporte_personal",reporte.ListarReporte)
router.get("/reporte_personal/:userId/:fecha",reporte.BuscarReporte)
router.post("/reporte_personal",reporte.CrearReporte)
router.put("/reporte_personal/:id",reporte.ActualizarReporte)

module.exports = router