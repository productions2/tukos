const {Router} = require("express")
const detalle = require("../controllers/MenuDetalle.controller")
const router = Router()

router.get("/detalle_menu",detalle.ListarMenuDetalle)
router.post("/detalle_menu",detalle.CrearMenuDetalle)

module.exports = router