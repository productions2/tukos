const {Router} = require("express")
const turnos = require("../controllers/turno.controller")
const router = Router()

router.get("/turno",turnos.ListarTurnos)
router.get("/turno/:id",turnos.BuscarTurno)
router.post("/turno",turnos.CrearTurno)


module.exports = router