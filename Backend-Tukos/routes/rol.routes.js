const {Router} = require("express")
const rol = require("../controllers/rol.controller")
const router = Router()

router.get("/rol", rol.ListarRoles)
router.post("/rol", rol.CrearRol)
router.put("/rol/:id", rol.ActualizarEstado)

module.exports = router