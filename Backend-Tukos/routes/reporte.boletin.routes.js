const {Router} = require("express")
const boletin = require("../controllers/boletas.controller")

const router = Router()

router.get("/boletin",boletin.ListarBoletas)
router.post("/boletin",boletin.CrearBoleta)
router.post("/boletinDown", boletin.BuscarBoleta)

module.exports = router