const { Router } = require("express")
const usuario = require("../controllers/usuario.controller")
const router = Router()

    router.get("/usuario",usuario.ListarUsuario)
    router.get("/usuario/:id",usuario.BuscarUsuario)
    router.post("/usuario",usuario.CrearUsuario)
    router.post("/usuario/login",usuario.LoginUsuario)
    router.put("/usuario/info/:id", usuario.ActualizarInfoPersonal)
    router.put("/usuario/correo/:id", usuario.ActualizarInfoCorreo)
    router.put("/usuario/estado/:id",usuario.ActualizarEstadoUsuario)
    /*router.post("/usuario",usuario.IniciarUsuario)
    router.put("/usuario/:id", usuario.ActualizarUsuario)
    router.delete("/usuario/:id",usuario.EliminarUsuario)*/


module.exports = router