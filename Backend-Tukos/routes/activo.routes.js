const {Router} =  require("express")
const activo = require("../controllers/activo.controller")

const router = Router();

router.get("/activo", activo.ListarActivos)
router.get("/activo/:id", activo.BuscarActivo)
router.post("/activo", activo.CrearActivo)
router.put("/activo/:id",activo.EditarActivo)
router.delete("/activo/:id",activo.BorrarActivo)

module.exports =  router