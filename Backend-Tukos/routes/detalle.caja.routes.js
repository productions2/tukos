const {Router} = require("express")
const DetalleCaja = require("../controllers/detalleCaja.controller")
const router = Router()

router.get("/detalleCaja",DetalleCaja.ListarDetalleCaja)
router.post("/detalleCaja",DetalleCaja.CrearDetalleCaja)
router.get("/reportedetallecaja/:userId",DetalleCaja.ReporteCajaDetalles)


module.exports = router