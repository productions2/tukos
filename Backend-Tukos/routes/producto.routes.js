const {Router} = require("express")
const producto = require("../controllers/producto.controller")
const router = Router()

router.get("/producto",producto.ListarProducto)
router.get("/producto/:id", producto.BuscarProducto)
router.post("/producto",producto.CrearProductos)
router.put("/producto/:id", producto.ActualizarProducto)

module.exports = router