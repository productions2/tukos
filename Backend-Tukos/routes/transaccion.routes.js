const {Router}= require("express")
const transaccion = require("../controllers/transaccion.controller")
const router = Router()

router.get("/transaccion",transaccion.ListarTransaccion)
router.post("/transaccion", transaccion.CrearTransaccion)

module.exports = router