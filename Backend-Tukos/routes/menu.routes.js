const {Router} = require("express")
const menu = require("../controllers/menu.controller")
const router = Router()

router.get("/menu",menu.ListarMenu)
router.get("/menu/:id",menu.BuscarMenu)
router.post("/menu",menu.CrearMenu)
router.put("/menu/:id",menu.EstadoMenu)
router.put("/menuUpdate/:id",menu.actualizarMenu)
router.delete("/menu/:id",menu.BorrarMenu)

module.exports = router