const {Router} = require("express")
const ventas = require("../controllers/reporte.ventas.controller")
const router = Router()

router.get("/ventas",ventas.ListarVentas)
router.post("/ventas",ventas.CrearVenta)

module.exports = router 