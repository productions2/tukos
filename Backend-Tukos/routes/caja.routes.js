const {Router} = require("express")
const cajas = require("../controllers/caja.controller")
const router = Router()

router.get("/caja",cajas.ListarCajas)
router.get("/caja/:userId",cajas.BuscarCaja)
router.get("/cajainicio",cajas.BuscarCajaInicio)
router.post("/caja",cajas.CrearCaja)
router.put("/caja/:id",cajas.ActualizarCaja)
router.get("/cajasUltima", cajas.UltimaCaja)
router.get("/reportecajas/:userId", cajas.ReporteCajas)
module.exports = router