const {Router} = require("express")
const tipo_habitacion = require("../controllers/tipo.habitacion.controller")
const router = Router()

router.get("/tipo_habitacion",tipo_habitacion.ListarTiposHabitacion)
router.get("/tipo_habitacion/:nombre",tipo_habitacion.BuscarTiposHabitacion)
router.get("/tipo_habitacionId/:id",tipo_habitacion.BuscarTiposHabitacionID)
router.post("/tipo_habitacion",tipo_habitacion.CrearTiposHabitacion)
router.put("/tipo_habitacion/:id",tipo_habitacion.EditarTiposHabitacion)
router.delete("/tipo_habitacion/:id",tipo_habitacion.BorrarTiposHabitacion)

module.exports = router