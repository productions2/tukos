const {Router} = require("express")
const ficha = require("../controllers/ficha.controller")

const router = Router()

router.get("/ficha",ficha.ListarFichas)
router.get("/ficha/:userId",ficha.BuscarFicha)
router.post("/ficha",ficha.CrearFicha)

module.exports = router