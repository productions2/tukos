const express =  require("express")
const cors =  require("cors")
const app = express()
const bodyParser = require('body-parser');
const verifyToken = require("./validate-token")


//middlewares
app.use(cors())
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


app.use("/api",require("./routes/usuario.routes"))
//app.use("/",)
app.use("/api",require("./routes/pais.routes"))
app.use("/api",require("./routes/persona.routes"))
app.use("/api",require("./routes/activo.routes"))
app.use("/api",require("./routes/habitacion.routes"))
app.use("/api",require("./routes/menu.routes"))
app.use("/api",require("./routes/detalle.menu.routes"))
app.use("/api",require("./routes/reporte.reserva.routes"))
app.use("/api",require("./routes/reporte.hospedaje.routes"))
app.use("/api",require("./routes/tipo.habitacion.routes"))
app.use("/api",require("./routes/turno.routes"))
app.use("/api",require("./routes/caja.routes"))
app.use("/api",require("./routes/detalle.caja.routes"))
app.use("/api",require("./routes/caracteristica.routes"))
app.use("/api",require("./routes/rol.routes"))
app.use("/api",require("./routes/reporte.boletin.routes"))
app.use("/api",require("./routes/reporte.ficha.routes"))
app.use("/api",require("./routes/reporte.personas.routes"))
app.use("/api",require("./routes/tareas.routes"))
app.use("/api",require("./routes/habitacion.activo.routes"))
app.use("/api",require("./routes/producto.routes"))
app.use("/api",require("./routes/reporte.ventas.routes"))
app.use("/api",require("./routes/transaccion.routes"))
//app.use(bodyparser.urlencoded({extended:false}))


//app.use("/api",users_routes)



module.exports  = app