"use strict";
const { DataTypes, JSONB } = require("sequelize");

// const { Activos } = require("../../models/activo.model");
// const { AdminCaja } = require("../../models/adminCaja.model");
// const { Cajas } = require("../../models/cajas.model");
// const {
//   Caracteristicas,
// } = require("../../models/caracteristicasHabitacion.model");
// const { DetalleCajas } = require("../../models/detalleCaja");
// const { Habitaciones } = require("../../models/habitacion.model");
// const { HabitacionActivos } = require("../../models/habitacionActivos.model");
// const { MenuHotel } = require("../../models/menuHotel");
// const { MenuReserva } = require("../../models/menuReserva.model");
// const { Paises } = require("../../models/pais.model");
// const { Personas } = require("../../models/persona.model");
// const { Productos } = require("../../models/producto.model");
// const { RegistroHospedaje } = require("../../models/registroHospedaje.model");
// const { RegistroReserva } = require("../../models/registroreserva.model");
// const { Boletas } = require("../../models/reporte.boletas.model");
// const { Fichas } = require("../../models/reporte.fichas.model");
// const { ReportePersonal } = require("../../models/reporte.personal.model");
// const { Roles } = require("../../models/rol.model");
// const { Tareas } = require("../../models/tareas.usuario.model");
// const { TiposHabitacion } = require("../../models/tipoHabitacion.model");
// const { Transaccion } = require("../../models/transaccion.model");
// const { Turnos } = require("../../models/turnos.model");
// const { Users } = require("../../models/user.model");
// const { Ventas } = require("../../models/venta.model");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.createTable(
      "activos",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nombre: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        codigo_activo: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
          validate: {
            notEmpty: true,
          },
        },
        marca: {
          type: DataTypes.STRING,
          validate: {
            notEmpty: true,
          },
        },
        estado: {
          type: DataTypes.ENUM,
          values: ["disponible", "ocupado", "mantenimiento"],
          defaultValue: "disponible",
          allowNull: false,
        },
        imagen: {
          type: DataTypes.TEXT("long"),
          allowNull: false,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "users",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          required: true,
          validate: {
            isEmail: {
              msg: "Must be a valid email address",
            },
          },
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
          required: true,
        },
        nombre_completo: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        ci: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        estado: {
          type: DataTypes.ENUM,
          values: ["Habilitado", "Deshabilitado"],
          defaultValue: "Deshabilitado",
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        rol: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        imagen: {
          type: DataTypes.TEXT("long"),
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "cajas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        monto_inicial: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        monto_reserva: {
          type: DataTypes.DOUBLE,
        },
        monto_cierre: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        estado: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "caracteristicas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        caracteristica: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        fecha_registro: {
          type: DataTypes.DATE,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "detalleCajas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        monto: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        tipo: {
          type: DataTypes.STRING,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "habitaciones",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        codigo_habitacion: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
        },
        caracteristicas: {
          type: DataTypes.ARRAY(DataTypes.STRING),
        },
        imagenes: {
          type: DataTypes.ARRAY(DataTypes.TEXT("long")),
        },
        precio_noche: {
          type: DataTypes.DOUBLE,
        },
        estado: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        piso: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        user_id: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "habitacionActivos",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        estado: {
          type: DataTypes.ENUM,
          values: ["activo", "baja"],
          defaultValue: "baja",
          allowNull: false,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "menuHotels",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        producto: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        precio_cu: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        precio_descuento: {
          type: DataTypes.DOUBLE,
        },
        estado: {
          type: DataTypes.BOOLEAN,
        },
        imagen: {
          type: DataTypes.TEXT("long"),
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "menuReservas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        cantidad: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notEmpty: true,
            isNumeric: true,
          },
        },
        precio: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        descuento: {
          type: DataTypes.DOUBLE,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        menuId: {
          type: DataTypes.INTEGER,
        },
        productoId: {
          type: DataTypes.INTEGER,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "paises",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        pais: {
          type: DataTypes.STRING,
          unique: true,
          validate: {
            notEmpty: true,
          },
        },
        abreviado: {
          type: DataTypes.STRING,
          unique: true,
          validate: {
            notEmpty: true,
          },
        },
        img: {
          type: DataTypes.TEXT,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "personas",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nacionalidad: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        ci_dni: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        nombres: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        apellidoP: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        apellidoM: {
          type: DataTypes.STRING,
        },
        genero: {
          type: DataTypes.ENUM,
          values: ["Femenino", "Masculino"],
          allowNull: false,
        },
        zona: {
          type: DataTypes.STRING,
        },
        direccion: {
          type: DataTypes.STRING,
        },
        profesion: {
          type: DataTypes.STRING,
        },
        contacto: {
          type: DataTypes.STRING,
        },
        fecha_nacimiento: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        estado_civil: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        estado: {
          type: DataTypes.ENUM,
          values: ["activo", "baja"],
          defaultValue: "baja",
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        foto: {
          type: DataTypes.TEXT,
        },
        fecha_registro: {
          type: DataTypes.DATE,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "productos",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nombre: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
          validate: {
            notEmpty: true,
          },
        },
        cantidad: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        precio: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        precio_descuento: {
          type: DataTypes.DOUBLE,
        },
        imagen: {
          type: DataTypes.TEXT("long"),
          allowNull: false,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "registrosHospedaje",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        dni_huespedes: {
          type: DataTypes.ARRAY(DataTypes.STRING),
          allowNull: false,
        },
        procedencia: {
          type: DataTypes.STRING,
        },
        cant_mayores: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        cant_menores: {
          type: DataTypes.INTEGER,
        },
        destino: {
          type: DataTypes.STRING,
        },
        trabajo: {
          type: DataTypes.BOOLEAN,
        },
        num_noches: {
          type: DataTypes.INTEGER,
        },
        pago_noche: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        pago_total: {
          type: DataTypes.DOUBLE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        fecha_ingreso: {
          type: DataTypes.DATEONLY,
          allowNull: false,
        },
        fecha_salida: {
          type: DataTypes.DATEONLY,
          allowNull: false,
        },
        estado: {
          type: DataTypes.BOOLEAN,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "registroReservas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        CiTitular: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        CiAcompañantes: {
          type: DataTypes.ARRAY(DataTypes.STRING),
        },
        procedencia: {
          type: DataTypes.STRING,
        },
        destino: {
          type: DataTypes.STRING,
        },
        cantidad_personas: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        trabajo: {
          type: DataTypes.BOOLEAN,
        },
        pago: {
          type: DataTypes.DOUBLE,
        },
        num_noches: {
          type: DataTypes.INTEGER,
        },
        saldo: {
          type: DataTypes.DOUBLE,
        },
        estado: {
          type: DataTypes.BOOLEAN,
        },
        tipo: {
          type: DataTypes.STRING,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        ultima_actualizacion: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        fechaingreso: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        fechasalida: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "boletas",
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        mes: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        año: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        dias: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        ingresos: {
          type: DataTypes.ARRAY(JSONB),
          allowNull: false,
        },
        descuentos: {
          type: DataTypes.ARRAY(JSONB),
          allowNull: false,
        },
        total: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        estado: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
        },
        fecha_registro: {
          type: DataTypes.DATE,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "fichas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tipo: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        asunto: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        descripcion: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        monto: {
          type: DataTypes.DOUBLE,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        adminId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "reportesPersonal",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        hora_ingreso: {
          type: DataTypes.TIME,
        },
        hora_salida: {
          type: DataTypes.TIME,
        },
        fecha: {
          type: DataTypes.DATEONLY,
          allowNull: false,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "roles",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        rol: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
        },
        estado: {
          type: DataTypes.BOOLEAN,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "tareas",
      {
        id: {
          type: DataTypes.BIGINT,
          primaryKey: true,
          autoIncrement: true,
        },
        descripcion: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        estado: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
        },
        fecha_registro: {
          type: DataTypes.DATEONLY,
          allowNull: false,
        },
        userId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        controles: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        llaves: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        tanque1: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        cajaId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        tanque2: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        tanque3: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "tiposHabitaciones",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nombre: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        descripcion: {
          type: DataTypes.STRING,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "Transaccion",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        monto: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        tipo: {
          type: DataTypes.STRING,
        },
        descripcion: {
          type: DataTypes.STRING,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "turnos",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        turno: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        hora_ingreso: {
          type: DataTypes.TIME,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        hora_salida: {
          type: DataTypes.TIME,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "users",
      {
        // Model attributes are defined here
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          required: true,
          validate: {
            isEmail: {
              msg: "Must be a valid email address",
            },
          },
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
          required: true,
        },
        nombre_completo: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        ci: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        estado: {
          type: DataTypes.ENUM,
          values: ["Habilitado", "Deshabilitado"],
          defaultValue: "Deshabilitado",
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        rol: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        imagen: {
          type: DataTypes.TEXT("long"),
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
    await queryInterface.createTable(
      "ventas",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        precio_total: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        descuento_total: {
          type: DataTypes.DOUBLE,
        },
        fecha_registro: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        timestamps: false,
      }
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
