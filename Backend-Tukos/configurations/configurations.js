const dotenv = require("dotenv");

dotenv.config();

const config = {
  env: process.env.NODE_ENV || "dev",
  port: process.env.PORT || 3001,
  dbUser: process.env.POSTGRES_USER,
  dbHost: process.env.POSTGRES_HOST,
  dbName: process.env.POSTGRES_DB,
  dbPassword: process.env.POSTGRES_PASSWORD,
  dbPort: process.env.POSTGRES_PORT,
};

module.exports = config;
